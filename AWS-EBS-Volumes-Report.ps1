
function Get-EBSDev-info {
aws s3 ls --profile cts-development
	
	# Get all EBS Volumes by profile and create list
	
	$EBSlist = (aws ec2 describe-volumes --profile cts-development) | ConvertFrom-Json | Select-Object -ExpandProperty Volumes


	# Parse Instance list to get data by instance
	$EBSlistdetails = $EBSlist | ForEach-Object {
		$propertiesE = [ordered]@{
			EBSVolumeID = $_.VolumeId  
            EBSTagEnvironment = ($_.Tags | Where-Object -Property Key -eq Environment).value
            EBSTagApplication = ($_.Tags | Where-Object -Property Key -eq Application).value
            EBSTagOwner = ($_.Tags | Where-Object -Property Key -eq Owner).value
            EBSAttachmentsDevice = $_.Attachments.Device
            EBSAttachmentsInstance = $_.Attachments.InstanceId 
            EBSAttachmentsState = $_.Attachments.State
            EBSSize = $_.Size
            EBSSnapshotID = $_.SnapshotId
            EBSState = $_.State 
            EBSiops = $_.iops
            EBSTags = $_.Tags.key -join '+'

		}
		New-Object -TypeName PSObject -Property $propertiesE
	}
	$EBSlistdetails | Sort-Object -Property EBSVolumeID | Export-Csv -Path 'c:\temp\EBSReports\EBS-Dev-report.csv'
}

Get-EBSDev-info

function Get-EBSTest-info {
aws s3 ls --profile cts-test
	
	# Get all EBS Volumes by profile and create list
	
	$EBSlist = (aws ec2 describe-volumes --profile cts-test) | ConvertFrom-Json | Select-Object -ExpandProperty Volumes


	# Parse Instance list to get data by instance
	$EBSlistdetails = $EBSlist | ForEach-Object {
		$propertiesE = [ordered]@{
			EBSVolumeID = $_.VolumeId  
            EBSTagEnvironment = ($_.Tags | Where-Object -Property Key -eq Environment).value
            EBSTagApplication = ($_.Tags | Where-Object -Property Key -eq Application).value
            EBSTagOwner = ($_.Tags | Where-Object -Property Key -eq Owner).value
            EBSAttachmentsDevice = $_.Attachments.Device
            EBSAttachmentsInstance = $_.Attachments.InstanceId 
            EBSAttachmentsState = $_.Attachments.State
            EBSSize = $_.Size
            EBSSnapshotID = $_.SnapshotId
            EBSState = $_.State 
            EBSiops = $_.iops
            EBSTags = $_.Tags.key -join '+'

		}
		New-Object -TypeName PSObject -Property $propertiesE
	}
	$EBSlistdetails | Sort-Object -Property EBSVolumeID | Export-Csv -Path 'c:\temp\EBSReports\EBS-Test-report.csv'
}

Get-EBSTest-info


function Get-EBSUSProd-info {
aws s3 ls --profile cts-USProd
	
	# Get all EBS Volumes by profile and create list
	
	$EBSlist = (aws ec2 describe-volumes --profile cts-USProd) | ConvertFrom-Json | Select-Object -ExpandProperty Volumes


	# Parse Instance list to get data by instance
	$EBSlistdetails = $EBSlist | ForEach-Object {
		$propertiesE = [ordered]@{
			EBSVolumeID = $_.VolumeId  
            EBSTagEnvironment = ($_.Tags | Where-Object -Property Key -eq Environment).value
            EBSTagApplication = ($_.Tags | Where-Object -Property Key -eq Application).value
            EBSTagOwner = ($_.Tags | Where-Object -Property Key -eq Owner).value
            EBSAttachmentsDevice = $_.Attachments.Device
            EBSAttachmentsInstance = $_.Attachments.InstanceId 
            EBSAttachmentsState = $_.Attachments.State
            EBSSize = $_.Size
            EBSSnapshotID = $_.SnapshotId
            EBSState = $_.State 
            EBSiops = $_.iops
            EBSTags = $_.Tags.key -join '+'

		}
		New-Object -TypeName PSObject -Property $propertiesE
	}
	$EBSlistdetails | Sort-Object -Property EBSVolumeID | Export-Csv -Path 'c:\temp\EBSReports\EBS-USProd-report.csv'
}

Get-EBSUSProd-info


function Get-EBSCAProd-info {
aws s3 ls --profile cts-CAProd
	
	# Get all EBS Volumes by profile and create list
	
	$EBSlist = (aws ec2 describe-volumes --profile cts-CAProd) | ConvertFrom-Json | Select-Object -ExpandProperty Volumes


	# Parse Instance list to get data by instance
	$EBSlistdetails = $EBSlist | ForEach-Object {
		$propertiesE = [ordered]@{
			EBSVolumeID = $_.VolumeId  
            EBSTagEnvironment = ($_.Tags | Where-Object -Property Key -eq Environment).value
            EBSTagApplication = ($_.Tags | Where-Object -Property Key -eq Application).value
            EBSTagOwner = ($_.Tags | Where-Object -Property Key -eq Owner).value
            EBSAttachmentsDevice = $_.Attachments.Device
            EBSAttachmentsInstance = $_.Attachments.InstanceId 
            EBSAttachmentsState = $_.Attachments.State
            EBSSize = $_.Size
            EBSSnapshotID = $_.SnapshotId
            EBSState = $_.State 
            EBSiops = $_.iops
            EBSTags = $_.Tags.key -join '+'

		}
		New-Object -TypeName PSObject -Property $propertiesE
	}
	$EBSlistdetails | Sort-Object -Property EBSVolumeID | Export-Csv -Path 'c:\temp\EBSReports\EBS-CAProd-report.csv'
}

Get-EBSCAProd-info


$files = Get-ChildItem 'c:\temp\EBSReports\*.csv'
$files | ForEach-Object { Import-Csv $_ } | Export-Csv -NoTypeInformation 'c:\temp\EBSReports\merged\CompuCom-AWS-EBSVolumes-Report.csv'
