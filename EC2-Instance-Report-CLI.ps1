# Generate an EC2 instance report
#
# I would recommend creating an access key profile in the SDK store â€“ by running the following in PowerShell.
# Set-AWSCredentials -AccessKey AccessKey -SecretKey SecretKey -StoreAs ProfileName
#  
# And to make things easier maybe associate a region with that profile.
# Initialize-AWSDefaultConfiguration -ProfileName ProfileName -Region us-east-1

if (Test-Path -Path 'c:\Temp\AWSReports\merged' -PathType Container)
{
	Write-Host "AWS Report folder already exist"
}
Else
{
	Write-Host "Creating Report folders c:\Temp\AWSReports\merged"
	
	New-Item -Path 'c:\Temp\AWSReports\merged' -Type Directory
	
}

if (Test-Path -Path 'c:\Temp\AWS-Snapshots' -PathType Container)
{
	Write-Host "AWS Snapshots Report folder already exist"
}
Else
{
	Write-Host "Creating Snapshots Report folders c:\Temp\AWS-Snapshots"
	
	New-Item -Path 'c:\Temp\AWS-Snapshots' -Type Directory
	
}

Function mfa
{
	do #while input does not match ARN format
	{
		#get user ARN
		$MFAInput = Read-Host "Please enter a valid AWS MFA Device ARN:"
	}
	while (!($MFAInput -like "*arn:aws:iam::*"))
	do #whilte MFA Code is not 6 digits
	{
		#get user MFA Code
		$MFACode = Read-Host "Please enter a valid 6 digit MFA Code:"
	}
	while ($MFACode.Length -ne 6)
	
	#Generate session info
	$sessionInfo = aws sts get-session-token --serial-number $($MFAInput) --token-code $($MFACode)
	
	
}

#mfa

function Get-SB-info
{
		#$DevProf = Read-Host "Please enter your Development Profile Name to Assume Role"
	#Start-Transcript -Path C:\Temp\AWS-Transcript.txt
	# Assume role
	aws s3 ls --profile cts-Sandbox #$DevProf
	
	# Get all EC2 Instances by profile and create list
	
	$ec2list = aws ec2 describe-instances --profile cts-Sandbox --query Reservations[*].Instances[*] | ConvertFrom-json #(Get-EC2Instance -ProfileName $DevProf).Instances 
	
	# Parse Instance list to get data by instance
	$ec2listdetails = $ec2list| ForEach-Object {
		$properties = [ordered]@{
			Name = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			Environment = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Environment).value
			Application = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Application).value
			InstanceState = $_.State.Name
			InstanceID = $_.InstanceId
			InstanceType = $_.InstanceType
			Platform = $_.Platform
			LaunchTime = $_.launchtime
			KeyName = $_.KeyName
			AmiID = $_.ImageID
			ImageName = (aws ec2 describe-images --profile cts-Sandbox --image-ids $_.imageID | Convertfrom-json | Select-Object -ExpandProperty Images).Name  # --query "Images[*]" --output json | Convertfrom-json | Select-Object -Property Name | Where-Object -Property imageID -eq $_.imageID).value #(Get-EC2Image -ProfileName $DevProf -ImageId $_.ImageID).Name
			PrivateIP = $_.PrivateIpAddress
			SubnetId = $_.SubnetId
			SubnetName = (aws ec2 describe-subnets --profile cts-Sandbox --subnet-ids $_.SubnetId | Convertfrom-json | Select-Object -ExpandProperty Subnets ).Tags.value #--query "Subnets[*]" --output json | Convertfrom-json | Where-Object -Property SubnetId -eq $_.SubnetId ).Tags.value #(Get-EC2Subnet -ProfileName $DevProf -subnetid ($_.SubnetId) | Select-Object -ExpandProperty tags |
			#	Where-Object -Property Key -eq Name).value
			NetworkInterfaceId = $_.networkinterfaces.networkinterfaceid
			MAC  = $_.networkinterfaces.MacAddress
			VPCId = $_.VpcId
			VPCName = (aws ec2 describe-vpcs --profile cts-Sandbox --vpc-id $_.VpcId | Convertfrom-json | Select-Object -ExpandProperty Vpcs ).Tags.value # --query "Vpcs[*]" --output json | Convertfrom-json | Where-Object -Property VpcId -eq $_.VpcId).Tags.value #(Get-EC2VPC -ProfileName $DevProf -vpcid ($_.vpcId) | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			AZ   = $_.placement.AvailabilityZone
			SG   = $_.SecurityGroups.GroupName -join '+'
			BackupTag = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq 'cpm backup').value
			volumeIDs = $_.blockdevicemappings.ebs.volumeid -join '+' | Select-Object -Unique
			
		}
		
		
		New-Object -TypeName PSObject -Property $properties
	}
	$ec2listdetails | Sort-Object -Property SubnetName | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-report.csv'

	$volumeIDs = $ec2list.blockdevicemappings.ebs.volumeid 
	
	
	# Get the latest snapshot for each volume
	$snapshots = ForEach ($volumeID in $volumeIDs)
	{
		
		$snapshotId = aws ec2 describe-snapshots --profile cts-Sandbox --filters Name=volume-id,Values=$volumeID --query Snapshots[*] | Convertfrom-json #Get-EC2Snapshot -ProfileName $DevProf | ? { $_.Tags.Key -eq "volumeid" -and $_.Tags.Value -eq $volumeID  }
		$snapshotcount = ($snapshotId).count
		
		$propertiesS = [ordered]@{
			volumeIDs = $volumeID
			snapshotIds = $snapshotId.SnapshotId -join '+'
			snapshotcreatedate = $snapshotId.StartTime -join '+'
			snapshotCount = $snapshotcount
			SnapVolumeSize = $snapshotId.VolumeSize | Select-Object -First 1
			
		}
		
		New-Object -TypeName PSObject -Property $propertiesS
		
	}
	
	
	$snapshots | Export-Csv -Path 'c:\temp\AWS-Snapshots\AWS-EC2-Sandbox-Snapshots.csv'
		
		# get ec2 instance right size recommendations by instance
		
		$RSR = aws compute-optimizer get-ec2-instance-recommendations --profile cts-Sandbox --region us-east-1
		$rsr1 = $RSR | ConvertFrom-Json | Select-Object -ExpandProperty instanceRecommendations
		
		$REC = $rsr1 | ForEach-Object {
			$properties2 = [ordered]@{
			Name = $_.InstanceName
			CPUPer = ($_ | Select-Object -ExpandProperty utilizationMetrics | Where-Object -Property name -eq 'cpu').value
			Finding = $_.finding
			Recomend = $_.recommendationOptions.instanceType[0]
				
			}
			
			New-Object -TypeName PSObject -Property $properties2
		
	}
	# Output results to file by profile 
	$REC | Export-Csv -Path 'c:\temp\AWSReports\ec2-SandboxREC-report.csv'
	# Merge result files
	$AWSR1 = Import-Csv 'c:\temp\AWSReports\ec2-Sandbox-report.csv'
	$AWSR2 = Import-Csv 'c:\temp\AWSReports\ec2-SandboxREC-report.csv'
	$AWSR3 = Import-Csv 'c:\temp\AWS-Snapshots\AWS-EC2-Sandbox-Snapshots.csv'
	ForEach ($Record in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record = Add-Member -InputObject $Record -Type NoteProperty -Name "Compute Optimize Findings" -Value $MatchedValue.Finding
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	ForEach ($Record2 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record2 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record2 = Add-Member -InputObject $Record2 -Type NoteProperty -Name "Compute Optimize Recomended Instance Type" -Value $MatchedValue.Recomend
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	ForEach ($Record3 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record3 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record3 = Add-Member -InputObject $Record3 -Type NoteProperty -Name "Current CPU %" -Value $MatchedValue.CPUPer
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	ForEach ($Record4 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record4 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record4 = Add-Member -InputObject $Record4 -Type NoteProperty -Name "Snapshot Count" -Value $MatchedValue.snapshotCount
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	ForEach ($Record5 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record5 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record5 = Add-Member -InputObject $Record5 -Type NoteProperty -Name "SnapshotIDs" -Value $MatchedValue.snapshotIds
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation

ForEach ($Record6 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record6 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record6 = Add-Member -InputObject $Record6 -Type NoteProperty -Name "SnapshotCreateDate" -Value $MatchedValue.snapshotcreatedate
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	ForEach ($Record7 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record7 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record7 = Add-Member -InputObject $Record7 -Type NoteProperty -Name "Snapshot Volume Size" -Value $MatchedValue.SnapVolumeSize
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Sandbox-full-report.csv' -NoTypeInformation
	
	# Remove redundant files before report merge
		Remove-Item -Path 'c:\temp\AWSReports\ec2-Sandbox-report.csv'
		Remove-Item -Path 'c:\temp\AWSReports\ec2-SandboxREC-report.csv'
		#Stop-Transcript
}

#Get-SB-info

function Get-Dev-info {
	#$DevProf = Read-Host "Please enter your Development Profile Name to Assume Role"
	#Start-Transcript -Path C:\Temp\AWS-Transcript.txt
	# Assume role
	aws s3 ls --profile cts-development #$DevProf
	
	# Get all EC2 Instances by profile and create list
	
	$ec2list = aws ec2 describe-instances --profile cts-development --query Reservations[*].Instances[*] | ConvertFrom-json #(Get-EC2Instance -ProfileName $DevProf).Instances 
	
	# Parse Instance list to get data by instance
	$ec2listdetails = $ec2list| ForEach-Object {
		$properties = [ordered]@{
			Name = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			Environment = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Environment).value
			Application = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Application).value
			InstanceState = $_.State.Name
			InstanceID = $_.InstanceId
			InstanceType = $_.InstanceType
			Platform = $_.Platform
			LaunchTime = $_.launchtime
			KeyName = $_.KeyName
			AmiID = $_.ImageID
			ImageName = (aws ec2 describe-images --profile cts-development --image-ids $_.imageID | Convertfrom-json | Select-Object -ExpandProperty Images).Name  # --query "Images[*]" --output json | Convertfrom-json | Select-Object -Property Name | Where-Object -Property imageID -eq $_.imageID).value #(Get-EC2Image -ProfileName $DevProf -ImageId $_.ImageID).Name
			PrivateIP = $_.PrivateIpAddress
			SubnetId = $_.SubnetId
			SubnetName = (aws ec2 describe-subnets --profile cts-development --subnet-ids $_.SubnetId | Convertfrom-json | Select-Object -ExpandProperty Subnets ).Tags.value #--query "Subnets[*]" --output json | Convertfrom-json | Where-Object -Property SubnetId -eq $_.SubnetId ).Tags.value #(Get-EC2Subnet -ProfileName $DevProf -subnetid ($_.SubnetId) | Select-Object -ExpandProperty tags |
			#	Where-Object -Property Key -eq Name).value
			NetworkInterfaceId = $_.networkinterfaces.networkinterfaceid
			MAC  = $_.networkinterfaces.MacAddress
			VPCId = $_.VpcId
			VPCName = (aws ec2 describe-vpcs --profile cts-development --vpc-id $_.VpcId | Convertfrom-json | Select-Object -ExpandProperty Vpcs ).Tags.value # --query "Vpcs[*]" --output json | Convertfrom-json | Where-Object -Property VpcId -eq $_.VpcId).Tags.value #(Get-EC2VPC -ProfileName $DevProf -vpcid ($_.vpcId) | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			AZ   = $_.placement.AvailabilityZone
			SG   = $_.SecurityGroups.GroupName -join '+'
			BackupTag = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq 'cpm backup').value
			volumeIDs = $_.blockdevicemappings.ebs.volumeid -join '+' | Select-Object -Unique
			
		}
		
		
		New-Object -TypeName PSObject -Property $properties
	}
	$ec2listdetails | Sort-Object -Property SubnetName | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-report.csv'

	$volumeIDs = $ec2list.blockdevicemappings.ebs.volumeid 
	
	
	# Get the latest snapshot for each volume
	$snapshots = ForEach ($volumeID in $volumeIDs)
	{
		
		$snapshotId = aws ec2 describe-snapshots --profile cts-development --filters Name=volume-id,Values=$volumeID --query Snapshots[*] | Convertfrom-json #Get-EC2Snapshot -ProfileName $DevProf | ? { $_.Tags.Key -eq "volumeid" -and $_.Tags.Value -eq $volumeID  }
		$snapshotcount = ($snapshotId).count
		
		$propertiesS = [ordered]@{
			volumeIDs = $volumeID
			snapshotIds = $snapshotId.SnapshotId -join '+'
			snapshotcreatedate = $snapshotId.StartTime -join '+'
			snapshotCount = $snapshotcount
			SnapVolumeSize = $snapshotId.VolumeSize | Select-Object -First 1
			
		}
		
		New-Object -TypeName PSObject -Property $propertiesS
		
	}
	
	
	$snapshots | Export-Csv -Path 'c:\temp\AWS-Snapshots\AWS-EC2-Dev-Snapshots.csv'
		
		# get ec2 instance right size recommendations by instance
		
		$RSR = aws compute-optimizer get-ec2-instance-recommendations --profile cts-development --region us-east-1
		$rsr1 = $RSR | ConvertFrom-Json | Select-Object -ExpandProperty instanceRecommendations
		
		$REC = $rsr1 | ForEach-Object {
			$properties2 = [ordered]@{
			Name = $_.InstanceName
			CPUPer = ($_ | Select-Object -ExpandProperty utilizationMetrics | Where-Object -Property name -eq 'cpu').value
			Finding = $_.finding
			Recomend = $_.recommendationOptions.instanceType[0]
				
			}
			
			New-Object -TypeName PSObject -Property $properties2
		
	}
	# Output results to file by profile 
	$REC | Export-Csv -Path 'c:\temp\AWSReports\ec2-DevREC-report.csv'
	# Merge result files
	$AWSR1 = Import-Csv 'c:\temp\AWSReports\ec2-Dev-report.csv'
	$AWSR2 = Import-Csv 'c:\temp\AWSReports\ec2-DevREC-report.csv'
	$AWSR3 = Import-Csv 'c:\temp\AWS-Snapshots\AWS-EC2-Dev-Snapshots.csv'
	ForEach ($Record in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record = Add-Member -InputObject $Record -Type NoteProperty -Name "Compute Optimize Findings" -Value $MatchedValue.Finding
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	ForEach ($Record2 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record2 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record2 = Add-Member -InputObject $Record2 -Type NoteProperty -Name "Compute Optimize Recomended Instance Type" -Value $MatchedValue.Recomend
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	ForEach ($Record3 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record3 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record3 = Add-Member -InputObject $Record3 -Type NoteProperty -Name "Current CPU %" -Value $MatchedValue.CPUPer
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	ForEach ($Record4 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record4 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record4 = Add-Member -InputObject $Record4 -Type NoteProperty -Name "Snapshot Count" -Value $MatchedValue.snapshotCount
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	ForEach ($Record5 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record5 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record5 = Add-Member -InputObject $Record5 -Type NoteProperty -Name "SnapshotIDs" -Value $MatchedValue.snapshotIds
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation

ForEach ($Record6 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record6 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record6 = Add-Member -InputObject $Record6 -Type NoteProperty -Name "SnapshotCreateDate" -Value $MatchedValue.snapshotcreatedate
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	ForEach ($Record7 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record7 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record7 = Add-Member -InputObject $Record7 -Type NoteProperty -Name "Snapshot Volume Size" -Value $MatchedValue.SnapVolumeSize
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-Dev-full-report.csv' -NoTypeInformation
	
	# Remove redundant files before report merge
		Remove-Item -Path 'c:\temp\AWSReports\ec2-Dev-report.csv'
		Remove-Item -Path 'c:\temp\AWSReports\ec2-DevREC-report.csv'
		#Stop-Transcript
}

Get-Dev-info

function Get-Test-info
{
	#$TestProf = Read-Host "Please enter your Test Profile Name to Assume Role"
	
	# Assume role
	aws s3 ls --profile cts-test #$TestProf
	# Get all EC2 Instances by profile and create list
	
	#$ec2list = (Get-EC2Instance -ProfileName $TestProf).Instances
	$ec2list = aws ec2 describe-instances --profile cts-test --query Reservations[*].Instances[*] | ConvertFrom-json #(Get-EC2Instance -ProfileName $DevProf).Instances 

	
	# Parse Instance list to get data by instance
	
	$ec2listdetails = $ec2list | ForEach-Object {
		$properties = [ordered]@{
			Name = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			Environment = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Environment).value
			Application = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Application).value
			InstanceState = $_.State.Name
			InstanceID = $_.InstanceId
			InstanceType = $_.InstanceType
			Platform = $_.Platform
			LaunchTime = $_.launchtime
			KeyName = $_.KeyName
			AmiID = $_.ImageID
			ImageName = (aws ec2 describe-images --profile cts-test --image-ids $_.imageID | Convertfrom-json | Select-Object -ExpandProperty Images).Name #(Get-EC2Image -ProfileName $TestProf -ImageId $_.ImageID).Name
			PrivateIP = $_.PrivateIpAddress
			SubnetId = $_.SubnetId
			SubnetName = (aws ec2 describe-subnets --profile cts-test --subnet-ids $_.SubnetId | Convertfrom-json | Select-Object -ExpandProperty Subnets ).Tags.value #(Get-EC2Subnet -ProfileName $TestProf -subnetid ($_.SubnetId) | Select-Object -ExpandProperty tags |
			#	Where-Object -Property Key -eq Name).value
			NetworkInterfaceId = $_.networkinterfaces.networkinterfaceid
			MAC  = $_.networkinterfaces.MacAddress
			VPCId = $_.VpcId
			VPCName = (aws ec2 describe-vpcs --profile cts-test --vpc-id $_.VpcId | Convertfrom-json | Select-Object -ExpandProperty Vpcs ).Tags.value #(Get-EC2VPC -ProfileName $TestProf -vpcid ($_.vpcId) | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			AZ   = $_.placement.AvailabilityZone
			SG   = $_.SecurityGroups.GroupName -join '+'
			BackupTag = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq 'cpm backup').value
			volumeIDs = $_.blockdevicemappings.ebs.volumeid -join '+' | Select-Object -Unique
			
		}
		
		
		New-Object -TypeName PSObject -Property $properties
	}
	$ec2listdetails | Sort-Object -Property SubnetName | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-report.csv'
	
	$volumeIDs = $ec2list.blockdevicemappings.ebs.volumeid 
			
	# Get the latest snapshot for each volume
	$snapshots = ForEach ($volumeID in $volumeIDs)
	{
		$snapshotId = aws ec2 describe-snapshots --profile cts-test --filters Name=volume-id,Values=$volumeID --query Snapshots[*] | Convertfrom-json #Get-EC2Snapshot -ProfileName $TestProf | ? { $_.Tags.Key -eq "volumeid" -and $_.Tags.Value -eq $volumeID }
		$snapshotcount = ($snapshotId).count
		
		$propertiesS = [ordered]@{
			volumeIDs	  = $volumeID
			snapshotIds = $snapshotId.SnapshotId -join '+'
			snapshotcreatedate = $snapshotId.StartTime -join '+'
			snapshotCount = $snapshotcount
			SnapVolumeSize = $snapshotId.VolumeSize | Select-Object -First 1
			
		}
		
		New-Object -TypeName PSObject -Property $propertiesS
		
	}
	

$snapshots | Export-Csv -Path 'c:\temp\AWS-Snapshots\AWS-EC2-Test-Snapshots.csv'
	
		# get ec2 instance right size recommendations by instance
		
		$RSR = aws compute-optimizer get-ec2-instance-recommendations --profile cts-test --region us-east-1
		$rsr1 = $RSR | ConvertFrom-Json | Select-Object -ExpandProperty instanceRecommendations
		
		$REC = $rsr1 | ForEach-Object {
			$properties2 = [ordered]@{
			Name = $_.InstanceName
			CPUPer = ($_ | Select-Object -ExpandProperty utilizationMetrics | Where-Object -Property name -eq 'cpu').value
			Finding = $_.finding
			Recomend = $_.recommendationOptions.instanceType[0]
				
			}
			
			New-Object -TypeName PSObject -Property $properties2
		}
		
	# Output results to file by profile 
	$REC | Export-Csv -Path 'c:\temp\AWSReports\ec2-testREC-report.csv'
	# Merge result files
	$AWSR1 = Import-Csv 'c:\temp\AWSReports\ec2-test-report.csv'
	$AWSR2 = Import-Csv 'c:\temp\AWSReports\ec2-testREC-report.csv'
	$AWSR3 = Import-Csv 'c:\temp\AWS-Snapshots\AWS-EC2-Test-Snapshots.csv'
	ForEach ($Record in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record = Add-Member -InputObject $Record -Type NoteProperty -Name "Compute Optimize Findings" -Value $MatchedValue.Finding
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	ForEach ($Record2 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record2 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record2 = Add-Member -InputObject $Record2 -Type NoteProperty -Name "Compute Optimize Recomended Instance Type" -Value $MatchedValue.Recomend
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	ForEach ($Record3 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record3 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record3 = Add-Member -InputObject $Record3 -Type NoteProperty -Name "Current CPU %" -Value $MatchedValue.CPUPer
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	ForEach ($Record4 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record4 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record4 = Add-Member -InputObject $Record4 -Type NoteProperty -Name "Snapshot Count" -Value $MatchedValue.snapshotCount
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	ForEach ($Record5 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record5 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record5 = Add-Member -InputObject $Record5 -Type NoteProperty -Name "SnapshotIDs" -Value $MatchedValue.snapshotIds
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation

ForEach ($Record6 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record6 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record6 = Add-Member -InputObject $Record6 -Type NoteProperty -Name "SnapshotCreateDate" -Value $MatchedValue.snapshotcreatedate
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	ForEach ($Record7 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record7 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record7 = Add-Member -InputObject $Record7 -Type NoteProperty -Name "Snapshot Volume Size" -Value $MatchedValue.SnapVolumeSize
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-test-full-report.csv' -NoTypeInformation
	
	
	# Remove redundant files before report merge
			Remove-Item -Path 'c:\temp\AWSReports\ec2-test-report.csv'
			Remove-Item -Path 'c:\temp\AWSReports\ec2-testREC-report.csv'
}

Get-Test-info

function Get-USProd-info
	{
	#$USProdProf = Read-Host "Please enter your US Production Profile Name to Assume Role"
	
	# Assume role
	aws s3 ls --profile cts-USProd #$USProdProf
	# Get all EC2 Instances by profile and create list
	#$ec2list = (Get-EC2Instance -ProfileName $USProdProf).Instances
	$ec2list = aws ec2 describe-instances --profile cts-USProd --query Reservations[*].Instances[*] | ConvertFrom-json #(Get-EC2Instance -ProfileName $DevProf).Instances 
	
	# Parse Instance list to get data by instance
	
	$ec2listdetails = $ec2list | ForEach-Object {
			$properties = [ordered]@{
			Name = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			Environment = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Environment).value
			Application = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Application).value
			InstanceState = $_.State.Name
			InstanceID = $_.InstanceId
			InstanceType = $_.InstanceType
			Platform = $_.Platform
			LaunchTime = $_.launchtime
			KeyName = $_.KeyName
			AmiID = $_.ImageID
			ImageName = (aws ec2 describe-images --profile cts-USProd --image-ids $_.imageID | Convertfrom-json | Select-Object -ExpandProperty Images).Name #(Get-EC2Image -ProfileName $USProdProf -ImageId $_.ImageID).Name
			PrivateIP = $_.PrivateIpAddress
			SubnetId = $_.SubnetId
			SubnetName = (aws ec2 describe-subnets --profile cts-USProd --subnet-ids $_.SubnetId | Convertfrom-json | Select-Object -ExpandProperty Subnets ).Tags.value #(Get-EC2Subnet -ProfileName $USProdProf -subnetid ($_.SubnetId) | Select-Object -ExpandProperty tags |
			#Where-Object -Property Key -eq Name).value
			NetworkInterfaceId = $_.networkinterfaces.networkinterfaceid
			MAC  = $_.networkinterfaces.MacAddress
			VPCId = $_.VpcId
			VPCName = (aws ec2 describe-vpcs --profile cts-USProd --vpc-id $_.VpcId | Convertfrom-json | Select-Object -ExpandProperty Vpcs ).Tags.value #(Get-EC2VPC -ProfileName $USProdProf -vpcid ($_.vpcId) | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			AZ   = $_.placement.AvailabilityZone
			SG   = $_.SecurityGroups.GroupName -join '+'
			BackupTag = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq 'cpm backup').value
			volumeIDs = $_.blockdevicemappings.ebs.volumeid -join '+' | Select-Object -Unique
			
		}
		
		New-Object -TypeName PSObject -Property $properties
	}
	$ec2listdetails | Sort-Object -Property SubnetName | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-report.csv'
	
	$volumeIDs = $ec2list.blockdevicemappings.ebs.volumeid
	
	
	# Get the latest snapshot for each volume
	$snapshots = ForEach ($volumeID in $volumeIDs)
	{
		$snapshotId = aws ec2 describe-snapshots --profile cts-USProd --filters Name=volume-id,Values=$volumeID --query Snapshots[*] | Convertfrom-json #Get-EC2Snapshot -ProfileName $USProdProf | ? { $_.Tags.Key -eq "volumeid" -and $_.Tags.Value -eq $volumeID }
		$snapshotcount = ($snapshotId).count
		
		$propertiesS = [ordered]@{
			volumeIDs   = $volumeID
			snapshotIds = $snapshotId.SnapshotId -join '+'
			snapshotcreatedate = $snapshotId.StartTime -join '+'
			snapshotCount = $snapshotcount
			SnapVolumeSize = $snapshotId.VolumeSize | Select-Object -First 1
			
		}
		
		New-Object -TypeName PSObject -Property $propertiesS
		
	}
	
	
	$snapshots | Export-Csv -Path 'c:\temp\AWS-Snapshots\AWS-EC2-USProd-Snapshots.csv'
	
		# get ec2 instance right size recommendations by instance
		
		$RSR = aws compute-optimizer get-ec2-instance-recommendations --profile cts-USProd --region us-east-1
		$rsr1 = $RSR | ConvertFrom-Json | Select-Object -ExpandProperty instanceRecommendations
		
		$REC = $rsr1 | ForEach-Object {
			$properties2 = [ordered]@{
			Name = $_.InstanceName
			CPUPer = ($_ | Select-Object -ExpandProperty utilizationMetrics | Where-Object -Property name -eq 'cpu').value
			Finding = $_.finding
			Recomend = $_.recommendationOptions.instanceType[0]
				
			}
			
			New-Object -TypeName PSObject -Property $properties2
		}
		
		
	# Output results to file by profile 
	$REC | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProdREC-report.csv'
	# Merge result files
	$AWSR1 = Import-Csv 'c:\temp\AWSReports\ec2-USProd-report.csv'
	$AWSR2 = Import-Csv 'c:\temp\AWSReports\ec2-USProdREC-report.csv'
	$AWSR3 = Import-Csv 'c:\temp\AWS-Snapshots\AWS-EC2-USProd-Snapshots.csv'

	ForEach ($Record in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record = Add-Member -InputObject $Record -Type NoteProperty -Name "Compute Optimize Findings" -Value $MatchedValue.Finding
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record2 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record2 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record2 = Add-Member -InputObject $Record2 -Type NoteProperty -Name "Compute Optimize Recomended Instance Type" -Value $MatchedValue.Recomend
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record3 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record3 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record3 = Add-Member -InputObject $Record3 -Type NoteProperty -Name "Current CPU %" -Value $MatchedValue.CPUPer
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record4 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record4 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record4 = Add-Member -InputObject $Record4 -Type NoteProperty -Name "Snapshot Count" -Value $MatchedValue.snapshotCount
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record5 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record5 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record5 = Add-Member -InputObject $Record5 -Type NoteProperty -Name "SnapshotIDs" -Value $MatchedValue.snapshotIds
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation

ForEach ($Record6 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record6 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record6 = Add-Member -InputObject $Record6 -Type NoteProperty -Name "SnapshotCreateDate" -Value $MatchedValue.snapshotcreatedate
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record7 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record7 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record7 = Add-Member -InputObject $Record7 -Type NoteProperty -Name "Snapshot Volume Size" -Value $MatchedValue.SnapVolumeSize
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-USProd-full-report.csv' -NoTypeInformation
	
	
	# Remove redundant files before report merge
	Remove-Item -Path 'c:\temp\AWSReports\ec2-USProd-report.csv'
	Remove-Item -Path 'c:\temp\AWSReports\ec2-USProdREC-report.csv'
	
}
	
	Get-USProd-info
	
	function Get-CAProd-info
{
	#$CAProdProf = Read-Host "Please enter your Canadian Production Profile Name to Assume Role"
	
	# Assume role
	aws s3 ls --profile cts-CAProd --region ca-central-1
	# Get all EC2 Instances by profile and create list
	#$ec2list = (Get-EC2Instance -ProfileName $CAProdProf -Region ca-central-1).Instances

	$ec2list = aws ec2 describe-instances --profile cts-CAProd --region ca-central-1 --query Reservations[*].Instances[*] | ConvertFrom-json #(Get-EC2Instance -ProfileName $DevProf).Instances 
	
	# Parse Instance list to get data by instance
	
	$ec2listdetails = $ec2list | ForEach-Object {
		$properties = [ordered]@{
			Name = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			Environment = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Environment).value
			Application = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Application).value
			InstanceState = $_.State.Name
			InstanceID = $_.InstanceId
			InstanceType = $_.InstanceType
			Platform = $_.Platform
			LaunchTime = $_.launchtime
			KeyName = $_.KeyName
			AmiID = $_.ImageID
			ImageName = (aws ec2 describe-images --profile cts-CAProd --region ca-central-1 --image-ids $_.imageID | Convertfrom-json | Select-Object -ExpandProperty Images).Name #(Get-EC2Image -ProfileName $CAProdProf -Region ca-central-1 -ImageId $_.ImageID).Name
			PrivateIP = $_.PrivateIpAddress
			SubnetId = $_.SubnetId
			SubnetName = (aws ec2 describe-subnets --profile cts-CAProd --region ca-central-1 --subnet-ids $_.SubnetId | Convertfrom-json | Select-Object -ExpandProperty Subnets ).Tags.value #(Get-EC2Subnet -ProfileName $CAProdProf -Region ca-central-1 -subnetid ($_.SubnetId) | Select-Object -ExpandProperty tags |
				#Where-Object -Property Key -eq Name).value
			NetworkInterfaceId = $_.networkinterfaces.networkinterfaceid
			MAC  = $_.networkinterfaces.MacAddress
			VPCId = $_.VpcId
			VPCName = (aws ec2 describe-vpcs --profile cts-CAProd --region ca-central-1 --vpc-id $_.VpcId | Convertfrom-json | Select-Object -ExpandProperty Vpcs ).Tags.value #(Get-EC2VPC -ProfileName $CAProdProf -Region ca-central-1 -vpcid ($_.vpcId) | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
			AZ   = $_.placement.AvailabilityZone
			SG   = $_.SecurityGroups.GroupName -join '+'
			BackupTag = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq 'cpm backup').value
			volumeIDs = $_.blockdevicemappings.ebs.volumeid -join '+' | Select-Object -Unique
			
		}
		
		New-Object -TypeName PSObject -Property $properties
	}
	$ec2listdetails | Sort-Object -Property SubnetName | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-report.csv'
	
	$volumeIDs = $ec2list.blockdevicemappings.ebs.volumeid
	
	
	# Get the latest snapshot for each volume
	$snapshots = ForEach ($volumeID in $volumeIDs)
	{
		$snapshotId = aws ec2 describe-snapshots --profile cts-CAProd --region ca-central-1 --filters Name=volume-id,Values=$volumeID --query Snapshots[*] | Convertfrom-json #Get-EC2Snapshot -ProfileName $CAProdProf -Region ca-central-1 | ? { $_.Tags.Key -eq "volumeid" -and $_.Tags.Value -eq $volumeID }
		$snapshotcount = ($snapshotId).count
		
		$propertiesS = [ordered]@{
			volumeIDs   = $volumeID
			snapshotIds = $snapshotId.SnapshotId -join '+'
			snapshotcreatedate = $snapshotId.StartTime -join '+'
			snapshotCount = $snapshotcount
			SnapVolumeSize = $snapshotId.VolumeSize | Select-Object -First 1
			
		}
		
		New-Object -TypeName PSObject -Property $propertiesS
		
	}
	
	
	$snapshots | Export-Csv -Path 'c:\temp\AWS-Snapshots\AWS-EC2-CAProd-Snapshots.csv'
	
	# get ec2 instance right size recommendations by instance
	
	$RSR = aws compute-optimizer get-ec2-instance-recommendations --profile cts-CAProd --region ca-central-1
	$rsr1 = $RSR | ConvertFrom-Json | Select-Object -ExpandProperty instanceRecommendations
	
	$REC = $rsr1 | ForEach-Object {
		$properties2 = [ordered]@{
			Name = $_.InstanceName
			CPUPer = ($_ | Select-Object -ExpandProperty utilizationMetrics | Where-Object -Property name -eq 'cpu').value
			Finding = $_.finding
			Recomend = $_.recommendationOptions.instanceType[0]
			
		}
		
		New-Object -TypeName PSObject -Property $properties2
	}
	
	
	# Output results to file by profile 
	$REC | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProdREC-report.csv'
	# Merge result files
	$AWSR1 = Import-Csv 'c:\temp\AWSReports\ec2-CAProd-report.csv'
	$AWSR2 = Import-Csv 'c:\temp\AWSReports\ec2-CAProdREC-report.csv'
	$AWSR3 = Import-Csv 'c:\temp\AWS-Snapshots\AWS-EC2-CAProd-Snapshots.csv'
	
	ForEach ($Record in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record = Add-Member -InputObject $Record -Type NoteProperty -Name "Compute Optimize Findings" -Value $MatchedValue.Finding
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record2 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record2 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record2 = Add-Member -InputObject $Record2 -Type NoteProperty -Name "Compute Optimize Recomended Instance Type" -Value $MatchedValue.Recomend
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record3 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR2 $Record3 -Property "Name" -IncludeEqual -ExcludeDifferent -PassThru
		$Record3 = Add-Member -InputObject $Record3 -Type NoteProperty -Name "Current CPU %" -Value $MatchedValue.CPUPer
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record4 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record4 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record4 = Add-Member -InputObject $Record4 -Type NoteProperty -Name "Snapshot Count" -Value $MatchedValue.snapshotCount
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record5 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record5 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record5 = Add-Member -InputObject $Record5 -Type NoteProperty -Name "SnapshotIDs" -Value $MatchedValue.snapshotIds
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation

ForEach ($Record6 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record6 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record6 = Add-Member -InputObject $Record6 -Type NoteProperty -Name "SnapshotCreateDate" -Value $MatchedValue.snapshotcreatedate
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	ForEach ($Record7 in $AWSR1)
	{
		$MatchedValue = Compare-Object $AWSR3 $Record7 -Property "volumeIDs" -IncludeEqual -ExcludeDifferent -PassThru
		$Record7 = Add-Member -InputObject $Record7 -Type NoteProperty -Name "Snapshot Volume Size" -Value $MatchedValue.SnapVolumeSize
		
	}
	$AWSR1 | Export-Csv -Path 'c:\temp\AWSReports\ec2-CAProd-full-report.csv' -NoTypeInformation
	
	
	# Remove redundant files before report merge
	Remove-Item -Path 'c:\temp\AWSReports\ec2-CAProd-report.csv'
	Remove-Item -Path 'c:\temp\AWSReports\ec2-CAProdREC-report.csv'
	
	}

Get-CAProd-info

# Merge Output file results to single report 

$files = Get-ChildItem 'c:\temp\AWSReports\*.csv'
$files | ForEach-Object { Import-Csv $_ } | Export-Csv -NoTypeInformation 'c:\temp\AWSReports\merged\CompuCom-AWS-Optimize-Report.csv'

# SIG # Begin signature block
# MIIqTgYJKoZIhvcNAQcCoIIqPzCCKjsCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCDwFuVbjzfTqQwn
# yy0JlRpSdUXEZifhuCtKHpdn6r0eNqCCI+0wggNfMIICR6ADAgECAgsEAAAAAAEh
# WFMIojANBgkqhkiG9w0BAQsFADBMMSAwHgYDVQQLExdHbG9iYWxTaWduIFJvb3Qg
# Q0EgLSBSMzETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UEAxMKR2xvYmFsU2ln
# bjAeFw0wOTAzMTgxMDAwMDBaFw0yOTAzMTgxMDAwMDBaMEwxIDAeBgNVBAsTF0ds
# b2JhbFNpZ24gUm9vdCBDQSAtIFIzMRMwEQYDVQQKEwpHbG9iYWxTaWduMRMwEQYD
# VQQDEwpHbG9iYWxTaWduMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
# zCV2kHkGeCIW9cCDtoTKKJ79BXYRxa2IcvxGAkPHsoqdBF8kyy5L4WCCRuFSqwyB
# R3Bs3WTR6/Usow+CPQwrrpfXthSGEHm7OxOAd4wI4UnSamIvH176lmjfiSeVOJ8G
# 1z7JyyZZDXPesMjpJg6DFcbvW4vSBGDKSaYo9mk79svIKJHlnYphVzesdBTcdOA6
# 7nIvLpz70Lu/9T0A4QYz6IIrrlOmOhZzjN1BDiA6wLSnoemyT5AuMmDpV8u5BJJo
# aOU4JmB1sp93/5EU764gSfytQBVI0QIxYRleuJfvrXe3ZJp6v1/BE++bYvsNbOBU
# aRapA9pu6YOTcXbGaYWCFwIDAQABo0IwQDAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0T
# AQH/BAUwAwEB/zAdBgNVHQ4EFgQUj/BLf6guRSSuTVD6Y5qL3uLdG7wwDQYJKoZI
# hvcNAQELBQADggEBAEtA28BQqv7IDO/3llRFSbuWAAlBrLMThoYoBzPKa+Z0uboA
# La6kCtP18fEPir9zZ0qDx0R7eOCvbmxvAymOMzlFw47kuVdsqvwSluxTxi3kJGy5
# lGP73FNoZ1Y+g7jPNSHDyWj+ztrCU6rMkIrp8F1GjJXdelgoGi8d3s0AN0GP7URt
# 11Mol37zZwQeFdeKlrTT3kwnpEwbc3N29BeZwh96DuMtCK0KHCz/PKtVDg+Rfjbr
# w1dJvuEuLXxgi8NBURMjnc73MmuUAaiZ5ywzHzo7JdKGQM47LIZ4yWEvFLru21Vv
# 34TuBQlNvSjYcs7TYlBlHuuSl4Mx2bO1ykdYP18wggQ+MIIDJqADAgECAgRKU4wo
# MA0GCSqGSIb3DQEBCwUAMIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVz
# dCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJt
# czE5MDcGA1UECxMwKGMpIDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3Jp
# emVkIHVzZSBvbmx5MTIwMAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlv
# biBBdXRob3JpdHkgLSBHMjAeFw0wOTA3MDcxNzI1NTRaFw0zMDEyMDcxNzU1NTRa
# MIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVzdCwgSW5jLjEoMCYGA1UE
# CxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJtczE5MDcGA1UECxMwKGMp
# IDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3JpemVkIHVzZSBvbmx5MTIw
# MAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkgLSBH
# MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALqEtnLbngxr4pnpMAGn
# duoyuJVBGsnaYU5Ycs/+9oJ5v3NhBgqlJ9izX9NFThxy1k4y8nKKD/eDGdBqgIAA
# RR6wx+eavxJXJxyjaC8Kh71qaw5eZfMcd9XUhY1wIbSzMueLotWGOQKxuNJHzuTJ
# ScQ7p977VH1XvvDobsJ5sjoLVeJQmBYyE1wveFbBwpSz8lrkJ5qfJNfG7NCbJYLj
# zMLERcWMl3oGayoRn6kKbkg7b9vUERlC948Hv/VTX5w+9Bcs5mmsTjJMYnfqt+jl
# uzS8GYuunFHnt361U7EzIuVtz3A8Gvrim2e2g/SNpa9iTE3gWKxkNBID+LaNlGMk
# pHECAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
# VR0OBBYEFGpyJnrQHu995ztpUdRsjZ+QEmarMA0GCSqGSIb3DQEBCwUAA4IBAQB5
# nx2WxrZ5PyKNh9OHAwRgamuaLlmJcxGsQ9H1E/+NOSvA8r1PcIypL+oXxAtUntQb
# lpgzPKitYqIAdqtZaW4GHX7EuUSNmK8S1GHbChlGR/Pr92PBQAVApdK39LWaNr+p
# iHaIBFUEK5yHfxo3PH4tpRrY1Ileyr2sPWzYba/V83YPzTuIOCKdbJOaxD2/ghtl
# P6YPXar85bIVyrWtxrw90ITo6gZysE05Mni/PhGcC6SdmiHz8JsLMHjbwdyHQ/68
# Y5rKxcIcyceN/zsSWAjmtj3seixO+4OWzgw8aYdUc6RzwpP/URCsFVQB2PwFsYmh
# f3SDmknX3E57ikhvi0X2MIIFEjCCA/qgAwIBAgIQe8CWLo22wYoAAAAAVWaoxjAN
# BgkqhkiG9w0BAQsFADCBtDELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3Qs
# IEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMx
# OTA3BgNVBAsTMChjKSAyMDE1IEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXpl
# ZCB1c2Ugb25seTEoMCYGA1UEAxMfRW50cnVzdCBDb2RlIFNpZ25pbmcgQ0EgLSBP
# VkNTMTAeFw0xOTEwMjQxNjQ1MDRaFw0yMjEwMjQxNzE0NTRaMHAxCzAJBgNVBAYT
# AlVTMQ4wDAYDVQQIEwVUZXhhczEPMA0GA1UEBxMGRGFsbGFzMR8wHQYDVQQKExZD
# b21wdUNvbSBTeXN0ZW1zLCBJbmMuMR8wHQYDVQQDExZDb21wdUNvbSBTeXN0ZW1z
# LCBJbmMuMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA07NoB1Uer8oe
# WARuBnhwFjfAvvMVVFvehylhOiY7W/4wbT5zXjcIHBW2sjwsCuvzd3t2x1qNDSMU
# T+m+HcYuhJw7CihBfKExUXPn1DVzDiBSuV8ib1IkXA/8ydyahwY8neOqajziodH9
# l53/OlcCTlddDSeG8j01mSbPRbjkKgIoj4Q7wnyBhX8mi3GAhtXnEk+mQLhRvYNO
# eRnkyt5DOn1N0Z/DENMK6AEmTGaYRikdl2ecntNq7usDzfjhpRhCiA7X5Gn17xKm
# 76OO3SVsLG325BraWNkI/BEfi940ybdetmfbXSyVqIJ7w7hEG87gqi83TImB+6/4
# 2iLlKBp18QIDAQABo4IBYTCCAV0wDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoG
# CCsGAQUFBwMDMGoGCCsGAQUFBwEBBF4wXDAjBggrBgEFBQcwAYYXaHR0cDovL29j
# c3AuZW50cnVzdC5uZXQwNQYIKwYBBQUHMAKGKWh0dHA6Ly9haWEuZW50cnVzdC5u
# ZXQvb3ZjczEtY2hhaW4yNTYuY2VyMDEGA1UdHwQqMCgwJqAkoCKGIGh0dHA6Ly9j
# cmwuZW50cnVzdC5uZXQvb3ZjczEuY3JsMEwGA1UdIARFMEMwNwYKYIZIAYb6bAoB
# AzApMCcGCCsGAQUFBwIBFhtodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYSAwCAYG
# Z4EMAQQBMB8GA1UdIwQYMBaAFH4aHxoRdFxkyQwflAGr/YFkLqEsMB0GA1UdDgQW
# BBR4BhTXpSBb0vAxAGlmhCm2A6ALAzAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUA
# A4IBAQBlQj3DomT1S72glQqQ/2Dx1W/U4K0P3pSqKW+xepzV4gQ+Q+EkNDTPN57b
# i7WmjT7ORmeSRaddfb5m19X3LqsCA0tqpyEQKQSODD5J0lUL4/l2kUPxmY+fLva5
# ug2uCBk2jo3dfWnYU4VNSrZ13aCYa+T+eHHccPxGVD5tFkzXJbpOFpKbfBYpzbGj
# WyCBEKaTs/80mE2F9QG6Dp9DjSvOuIorWfSBkBLxy8xoEbIMfPtvDXRZpjuYlc/E
# /2gKF07JgC3bOsRt1dbgwrAy8yOw/O30xdDXklqflFeMk8aTq3X7CstAqcQSLaHn
# mwY5Ai1vKG88l2hJxuXtRgqN+0kXMIIFHTCCBAWgAwIBAgIMQ8ELHAAAAABR03Pa
# MA0GCSqGSIb3DQEBCwUAMIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVz
# dCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJt
# czE5MDcGA1UECxMwKGMpIDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3Jp
# emVkIHVzZSBvbmx5MTIwMAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlv
# biBBdXRob3JpdHkgLSBHMjAeFw0xNTA2MTAxMzQ2MDVaFw0zMDExMTAxNDE2MDVa
# MIG0MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVzdCwgSW5jLjEoMCYGA1UE
# CxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJtczE5MDcGA1UECxMwKGMp
# IDIwMTUgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3JpemVkIHVzZSBvbmx5MSgw
# JgYDVQQDEx9FbnRydXN0IENvZGUgU2lnbmluZyBDQSAtIE9WQ1MxMIIBIjANBgkq
# hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2IYNoGQKavWOlro7mmhq20RA3vUt/Ott
# RFjPjnLDcC1eWtT5C9gRNZfAKfE77heh1kS5vx9KmOAZDrXjWQgdUoygsIooQaSh
# hkBQs03HiEHaNpOnTD1jLAPLr40AJRFpUCL2SwDOw7IugYXdI91OTbXwwKh0/Ots
# YCmYcY9o97cd8/hDi2KVMoZPyshMS1P+USAwHv8ARMdecZqlG4l0Sv5TPoTqsaOJ
# nfgtQljYCgZQI3AWHAJ3lxg82RB9DHrmBSwaCBZmEjhscR3WZNdWwI02w7i1ALjd
# L7V6v7Gkbnj6bRJ25Jl3EaYk5IK5RnmpsKNJW9NTdEOBdG+h+C4vJQIDAQABo4IB
# ITCCAR0wDgYDVR0PAQH/BAQDAgEGMBMGA1UdJQQMMAoGCCsGAQUFBwMDMBIGA1Ud
# EwEB/wQIMAYBAf8CAQAwMwYIKwYBBQUHAQEEJzAlMCMGCCsGAQUFBzABhhdodHRw
# Oi8vb2NzcC5lbnRydXN0Lm5ldDAwBgNVHR8EKTAnMCWgI6Ahhh9odHRwOi8vY3Js
# LmVudHJ1c3QubmV0L2cyY2EuY3JsMDsGA1UdIAQ0MDIwMAYEVR0gADAoMCYGCCsG
# AQUFBwIBFhpodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYTAdBgNVHQ4EFgQUfhof
# GhF0XGTJDB+UAav9gWQuoSwwHwYDVR0jBBgwFoAUanImetAe733nO2lR1GyNn5AS
# ZqswDQYJKoZIhvcNAQELBQADggEBALd0Z7Q+TCUvYjWBnNJ25afiKHh0w+wZ5RP5
# tiOUT2KnTOZOcsIkkNB5uqjN10TfADkw8SjEcm94rJALLaPqha0UwgsUmT/7I3St
# qKrMImMyJQErytAWRkJap0RHWB0EQeLRMb6XYTuuBBhsenqBX6si/gjWNY6nDfXs
# INzcbceidtj7ZqhDAwEhqKD2TObUgs7XDQHWM6yQo+CSuxgCdUMCegS02BXtV5E8
# EueNZvi9FMxGgtb6A8AKDXyxgA/roRo2pXUfPNg66ueVgd6sWvudtcwRTip0EdZ/
# eUJhyiFBYB6k3fHZdLDVpegLTIGJ38yGFiUZpRRXlaKKInuxZY0wggVHMIIEL6AD
# AgECAg0B8kBCQM79ItvpbHH8MA0GCSqGSIb3DQEBDAUAMEwxIDAeBgNVBAsTF0ds
# b2JhbFNpZ24gUm9vdCBDQSAtIFIzMRMwEQYDVQQKEwpHbG9iYWxTaWduMRMwEQYD
# VQQDEwpHbG9iYWxTaWduMB4XDTE5MDIyMDAwMDAwMFoXDTI5MDMxODEwMDAwMFow
# TDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjYxEzARBgNVBAoTCkds
# b2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wggIiMA0GCSqGSIb3DQEBAQUA
# A4ICDwAwggIKAoICAQCVB+hzymb57BTKezz3DQjxtEULLIK0SMbrWzyug7hBkjMU
# pG9/6SrMxrCIa8W2idHGsv8UzlEUIexK3RtaxtaH7k06FQbtZGYLkoDKRN5zlE7z
# p4l/T3hjCMgSUG1CZi9NuXkoTVIaihqAtxmBDn7EirxkTCEcQ2jXPTyKxbJm1ZCa
# tzEGxb7ibTIGph75ueuqo7i/voJjUNDwGInf5A959eqiHyrScC5757yTu21T4kh8
# jBAHOP9msndhfuDqjDyqtKT285VKEgdt/Yyyic/QoGF3yFh0sNQjOvddOsqi250J
# 3l1ELZDxgc1Xkvp+vFAEYzTfa5MYvms2sjnkrCQ2t/DvthwTV5O23rL44oW3c6K4
# NapF8uCdNqFvVIrxclZuLojFUUJEFZTuo8U4lptOTloLR/MGNkl3MLxxN+Wm7CEI
# dfzmYRY/d9XZkZeECmzUAk10wBTt/Tn7g/JeFKEEsAvp/u6P4W4LsgizYWYJarEG
# OmWWWcDwNf3J2iiNGhGHcIEKqJp1HZ46hgUAntuA1iX53AWeJ1lMdjlb6vmlodiD
# D9H/3zAR+YXPM0j1ym1kFCx6WE/TSwhJxZVkGmMOeT31s4zKWK2cQkV5bg6HGVxU
# sWW2v4yb3BPpDW+4LtxnbsmLEbWEFIoAGXCDeZGXkdQaJ783HjIH2BRjPChMrwID
# AQABo4IBJjCCASIwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
# VR0OBBYEFK5sBaOTE+Ki5+LXHNbH8H/IZ1OgMB8GA1UdIwQYMBaAFI/wS3+oLkUk
# rk1Q+mOai97i3Ru8MD4GCCsGAQUFBwEBBDIwMDAuBggrBgEFBQcwAYYiaHR0cDov
# L29jc3AyLmdsb2JhbHNpZ24uY29tL3Jvb3RyMzA2BgNVHR8ELzAtMCugKaAnhiVo
# dHRwOi8vY3JsLmdsb2JhbHNpZ24uY29tL3Jvb3QtcjMuY3JsMEcGA1UdIARAMD4w
# PAYEVR0gADA0MDIGCCsGAQUFBwIBFiZodHRwczovL3d3dy5nbG9iYWxzaWduLmNv
# bS9yZXBvc2l0b3J5LzANBgkqhkiG9w0BAQwFAAOCAQEASaxexYPzWsthKk2XShUp
# n+QUkKoJ+cR6nzUYigozFW1yhyJOQT9tCp4YrtviX/yV0SyYFDuOwfA2WXnzjYHP
# dPYYpOThaM/vf2VZQunKVTm808Um7nE4+tchAw+3TtlbYGpDtH0J0GBh3artAF5O
# Mh7gsmyePLLCu5jTkHZqaa0a3KiJ2lhP0sKLMkrOVPs46TsHC3UKEdsLfCUn8awm
# zxFT5tzG4mE1MvTO3YPjGTrrwmijcgDIJDxOuFM8sRer5jUs+dNCKeZfYAOsQmGm
# sVdqM0LfNTGGyj43K9rE2iT1ThLytrm3R+q7IK1hFregM+Mtiae8szwBfyMagAk0
# 6TCCBlkwggRBoAMCAQICDQHsHJJA3v0uQF18R3QwDQYJKoZIhvcNAQEMBQAwTDEg
# MB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjYxEzARBgNVBAoTCkdsb2Jh
# bFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcNMTgwNjIwMDAwMDAwWhcNMzQx
# MjEwMDAwMDAwWjBbMQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBu
# di1zYTExMC8GA1UEAxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBTSEEz
# ODQgLSBHNDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAPAC4jAj+uAb
# 4Zp0s691g1+pR1LHYTpjfDkjeW10/DHkdBIZlvrOJ2JbrgeKJ+5Xo8Q17bM0x6zD
# DOuAZm3RKErBLLu5cPJyroz3mVpddq6/RKh8QSSOj7rFT/82QaunLf14TkOI/pMZ
# F9nuMc+8ijtuasSI8O6X9tzzGKBLmRwOh6cm4YjJoOWZ4p70nEw/XVvstu/SZc9F
# C1Q9sVRTB4uZbrhUmYqoMZI78np9/A5Y34Fq4bBsHmWCKtQhx5T+QpY78Quxf39G
# mA6HPXpl69FWqS69+1g9tYX6U5lNW3TtckuiDYI3GQzQq+pawe8P1Zm5P/RPNfGc
# D9M3E1LZJTTtlu/4Z+oIvo9Jev+QsdT3KRXX+Q1d1odDHnTEcCi0gHu9Kpu7hOEO
# rG8NubX2bVb+ih0JPiQOZybH/LINoJSwspTMe+Zn/qZYstTYQRLBVf1ukcW7sUwI
# S57UQgZvGxjVNupkrs799QXm4mbQDgUhrLERBiMZ5PsFNETqCK6dSWcRi4LlrVqG
# p2b9MwMB3pkl+XFu6ZxdAkxgPM8CjwH9cu6S8acS3kISTeypJuV3AqwOVwwJ0WGe
# Joj8yLJN22TwRZ+6wT9Uo9h2ApVsao3KIlz2DATjKfpLsBzTN3SE2R1mqzRzjx59
# fF6W1j0ZsJfqjFCRba9Xhn4QNx1rGhTfAgMBAAGjggEpMIIBJTAOBgNVHQ8BAf8E
# BAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQU6hbGaefjy1dFOTOk
# 8EC+0MO9ZZYwHwYDVR0jBBgwFoAUrmwFo5MT4qLn4tcc1sfwf8hnU6AwPgYIKwYB
# BQUHAQEEMjAwMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcDIuZ2xvYmFsc2lnbi5j
# b20vcm9vdHI2MDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly9jcmwuZ2xvYmFsc2ln
# bi5jb20vcm9vdC1yNi5jcmwwRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUH
# AgEWJmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMA0GCSqG
# SIb3DQEBDAUAA4ICAQB/4ojZV2crQl+BpwkLusS7KBhW1ky/2xsHcMb7CwmtADpg
# Mx85xhZrGUBJJQge5Jv31qQNjx6W8oaiF95Bv0/hvKvN7sAjjMaF/ksVJPkYROwf
# wqSs0LLP7MJWZR29f/begsi3n2HTtUZImJcCZ3oWlUrbYsbQswLMNEhFVd3s6Uqf
# XhTtchBxdnDSD5bz6jdXlJEYr9yNmTgZWMKpoX6ibhUm6rT5fyrn50hkaS/SmqFy
# 9vckS3RafXKGNbMCVx+LnPy7rEze+t5TTIP9ErG2SVVPdZ2sb0rILmq5yojDEjBO
# sghzn16h1pnO6X1LlizMFmsYzeRZN4YJLOJF1rLNboJ1pdqNHrdbL4guPX3x8pEw
# BZzOe3ygxayvUQbwEccdMMVRVmDofJU9IuPVCiRTJ5eA+kiJJyx54jzlmx7jqoSC
# iT7ASvUh/mIQ7R0w/PbM6kgnfIt1Qn9ry/Ola5UfBFg0ContglDk0Xuoyea+SKor
# VdmNtyUgDhtRoNRjqoPqbHJhSsn6Q8TGV8Wdtjywi7C5HDHvve8U2BRAbCAdwi3o
# C8aNbYy2ce1SIf4+9p+fORqurNIveiCx9KyqHeItFJ36lmodxjzK89kcv1NNpEdZ
# fJXEQ0H5JeIsEH6B+Q2Up33ytQn12GByQFCVINRDRL76oJXnIFm2eMakaqoimzCC
# BmUwggRNoAMCAQICEAGE06jON4HrV/T9h3uDrrIwDQYJKoZIhvcNAQELBQAwWzEL
# MAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExMTAvBgNVBAMT
# KEdsb2JhbFNpZ24gVGltZXN0YW1waW5nIENBIC0gU0hBMzg0IC0gRzQwHhcNMjEw
# NTI3MTAwMDE2WhcNMzIwNjI4MTAwMDE1WjBjMQswCQYDVQQGEwJCRTEZMBcGA1UE
# CgwQR2xvYmFsU2lnbiBudi1zYTE5MDcGA1UEAwwwR2xvYmFsc2lnbiBUU0EgZm9y
# IE1TIEF1dGhlbnRpY29kZSBBZHZhbmNlZCAtIEc0MIIBojANBgkqhkiG9w0BAQEF
# AAOCAY8AMIIBigKCAYEA4qKbtkn7NEgoIweG/VITTbrLqjXi+UAcG3tpxZgqzMlc
# 7yfG7LglZcM/HUTn3uEI7wQXAJWXDP3bXYO0iOugjptPg7r5314YVGoCDg7mVPA3
# pB6cZzEFtTdxfHp+2IFlM+Sa6rAxN8dmtKO6BSfqblKCWNmCViSN+euzFPtkAKF7
# Kh96nqhjkebY0SLGP9KdOD+vlWlbJ7/oQCSZxx/Ze5IFAn0oxjw593rN92UmoDev
# MgV3l2rhEBuYhSyEreUlhnxzns2IjUsqep0PWXm98to2fmasLvi7iJfJbXnys9TV
# zBr0T4tFlXbyh5WAjih5uv3FcK+8GhQCkHwmWxGLXfW65BECxYEcZmhL3mjOnQyI
# Yaq2lCOd4KQ8Rl0EUds/WQvn7L7S2RMqR+9LEhJuyDXP7hYyttBfhbhP8nzYCSsS
# XsDC7xMxcDZz3NZXVNNXYJ4cEJXw5qovEIobfsnNj85QSxwlqMAXZFEVC4zP18ld
# SmxJdK5a8WoGdT6JNzuRAgMBAAGjggGbMIIBlzAOBgNVHQ8BAf8EBAMCB4AwFgYD
# VR0lAQH/BAwwCgYIKwYBBQUHAwgwHQYDVR0OBBYEFHWvJz+425rsbxzuw2pk9r+E
# NbDOMEwGA1UdIARFMEMwQQYJKwYBBAGgMgEeMDQwMgYIKwYBBQUHAgEWJmh0dHBz
# Oi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMAkGA1UdEwQCMAAwgZAG
# CCsGAQUFBwEBBIGDMIGAMDkGCCsGAQUFBzABhi1odHRwOi8vb2NzcC5nbG9iYWxz
# aWduLmNvbS9jYS9nc3RzYWNhc2hhMzg0ZzQwQwYIKwYBBQUHMAKGN2h0dHA6Ly9z
# ZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzdHNhY2FzaGEzODRnNC5jcnQw
# HwYDVR0jBBgwFoAU6hbGaefjy1dFOTOk8EC+0MO9ZZYwQQYDVR0fBDowODA2oDSg
# MoYwaHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9jYS9nc3RzYWNhc2hhMzg0ZzQu
# Y3JsMA0GCSqGSIb3DQEBCwUAA4ICAQA4k7d9NYk06hrWqnrOjYTb4TS3dKX/2F2U
# NiWP0oB+oWWxm881UV3boEtu9yFALuxPbxZAchx1GzAAFzmPyC+kcc4kMyaQ1eT/
# Oblh7wvKOfMtXo2g/07hVmQf1Jx2BKREXD1ihXAv+U6k94ZW23pJJkMtBZviw4m3
# OqEtJycYxkOL3EOhtnItP9nGNIoC02I5KfB9KNCpw2SKRm2zQxx0i925pgshexGn
# H12i24zgVdNp+nfRXRS5luyRRRsbbH5CQCSk7UDGZfpBj0gxnOO4sxdXgnnKzB7C
# 0E9PBQ4Neddp3ZWhcV2Knudawk+oBg8IiXvbWMglff9ovCiicJy7DLVT6I4GsbKC
# gYcSqV53TJOrGPhEV1gRy/aTFUkxt1NQIdrV/GB/3TDFrFFEC2el+tZ3NK0SHyj7
# iNpRm0Sa3ncO7jISYFYMkZ1Yj7yZV/K+zbnDcyQZ2nem5Ozx3m/gvIQdb8Px2+HV
# QlGGURJCJj7T6hP0KJytTXq3oroUaj9AVVhOjGUcs/Z11n+uD4SAL76IeFwnH0cX
# wj3kaZ7Lrp7vAmiINxDCbCaKyIxlkFNNrl3auEYQ1JAKiv/0KECB4FLI8gFkSBiE
# eGoi+sqcqg4dxY5ywzYiGev4lB+/wsUBVummgOoBHRM1B+K5dBT9mjieA9JJ9kzr
# ob0cFNLjmTGCBbcwggWzAgEBMIHJMIG0MQswCQYDVQQGEwJVUzEWMBQGA1UEChMN
# RW50cnVzdCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdh
# bC10ZXJtczE5MDcGA1UECxMwKGMpIDIwMTUgRW50cnVzdCwgSW5jLiAtIGZvciBh
# dXRob3JpemVkIHVzZSBvbmx5MSgwJgYDVQQDEx9FbnRydXN0IENvZGUgU2lnbmlu
# ZyBDQSAtIE9WQ1MxAhB7wJYujbbBigAAAABVZqjGMA0GCWCGSAFlAwQCAQUAoEww
# GQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwLwYJKoZIhvcNAQkEMSIEIA3TMh65
# fv3ywF5rpl/R1Ih0JU8rgvzKIETNoSCLaNatMA0GCSqGSIb3DQEBAQUABIIBAMST
# 2OBBpiNe/iVJ+KyL6khavcXh64gKiuvoYHONUDU2s8nln6oSRYVpS/X1E2B1cp5F
# 809R4OgLdb83ucFM3xlFQ109zvXETKSiPVh40WMMsjyDXtSdY9xQlAt77S0bemIo
# TfXj4Y5AB7pn8hXwpzGNHkpVcyfD+8lE5N2kj2BmwnKED3vRDX2TFy/ySc2xkBZO
# 2W51yCjMj7eT4dQAOdLReJgDEgtGp/md5O6pqdKDL0IXHIw2cRiI0LxKQFtn8ov3
# KkF6cNpBqkvujewLbCNaaxo0mGS4tf0MR5RrDuHjf7p+OhvcMlxuopsA9iOmMPPV
# k9Wp/CTNRh7ehD+Fi+WhggNwMIIDbAYJKoZIhvcNAQkGMYIDXTCCA1kCAQEwbzBb
# MQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBudi1zYTExMC8GA1UE
# AxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBTSEEzODQgLSBHNAIQAYTT
# qM43getX9P2He4OusjANBglghkgBZQMEAgEFAKCCAT8wGAYJKoZIhvcNAQkDMQsG
# CSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjEwOTIxMjAzNDM3WjAtBgkqhkiG
# 9w0BCTQxIDAeMA0GCWCGSAFlAwQCAQUAoQ0GCSqGSIb3DQEBCwUAMC8GCSqGSIb3
# DQEJBDEiBCBlwR0/9IM80I3x7SzKSikHvnm3L4v452YRaWBRqOuRtjCBpAYLKoZI
# hvcNAQkQAgwxgZQwgZEwgY4wgYsEFN1XtbOHPIYbKcauxHMa++iNdcFJMHMwX6Rd
# MFsxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTEwLwYD
# VQQDEyhHbG9iYWxTaWduIFRpbWVzdGFtcGluZyBDQSAtIFNIQTM4NCAtIEc0AhAB
# hNOozjeB61f0/Yd7g66yMA0GCSqGSIb3DQEBCwUABIIBgAr7m/z8lG0P1214Yrm+
# y2d4eRj1z559/PHbok1kAugB1GzlUq+Ch8zvKbKSY9YWysdrfVjBH3QvOtJ9ktkp
# NvgGjz3indlIhfpFKzxPtn31mBl/WemZWZGDkyfpoT6pVfreC2OFWCvONMDBl7YW
# 7AVho8KlvoPCpvD3AM4kM0pOUzoCHb4F2laP8zuCOzldtFB0JFIbCV0KCFER1LZ9
# WeGREcF8vHUhM3XGGsFOTgfsb3jEgFIlPsaszK4RNgnGkXzxp2av1SNlPF/tRApp
# zGQ0Xp3e1nFyt5xoIiQdo1Os7/Ldbzo5Qx5AM4VcoJ+K6GYvYGprhXpGQjru5KY6
# 3MmBe7p4IYpETA+73cxGe2Py4tCOtmFLhjn2EB2gBXoVYtKLNhV73ZBe9v5cThPE
# GYzdoTska6uFKSedHVSLIJOo/4WkzsMV0evwQCuVY4g4Do0NasyobxvLqasZDTQj
# 4Z06Oj2sZeclcGzPuXCQFm5ka2oQ8f+AQQz7s7FbjNOa2w==
# SIG # End signature block