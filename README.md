# README #

This README describes the use and purpose of each script.

### What is this repository for? ###

* Quick summary
These scripts are for pulling information from the AWS space for EC2 instance 
optimization and reporting as well as gathering data related to snapshots EBS volumes and RDS instances.
* Version
1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Required - Powershell v5+ AWS Tools for powershell module
* Configuration
The config file for AWS needs to be configured per role with role arn
The Credintials file for AWS needs to be configured with a default role and access security controls as required
* Dependencies
powershell v5+
AWS Tools v4+
* Database configuration
* How to run tests
Run scripts from powershell configured for AWS
* Deployment instructions

### Contribution guidelines ###

* Writing tests
Keep all additions seperate from main code source
* Code review
A review with technology and AWS services should be done prior to promoting.
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Nicholas Oller - Technology
Brian Erwin - DevOps
* Other community or team contact