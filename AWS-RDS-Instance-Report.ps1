# Generate an AWS RDS instance report
#
# I would recommend creating an access key profile in the SDK store - by running the following in PowerShell.
# Set-AWSCredentials -AccessKey AccessKey -SecretKey SecretKey -StoreAs ProfileName
#  
# And to make things easier maybe associate a region with that profile.
# Initialize-AWSDefaultConfiguration -ProfileName ProfileName -Region us-east-1


function Get-RDSDev-info
{
	aws s3 ls --profile cts-development
	
	# Get all RDS Instances by profile and create list
	
	$RDSlist = (aws rds describe-db-instances --profile cts-development) | ConvertFrom-Json | Select-Object -ExpandProperty DBInstances
	
	
	# Parse Instance list to get data by instance
	$RDSlistdetails = $RDSlist | ForEach-Object {
		$properties = [ordered]@{
			DBName = $_.DBInstanceIdentifier
			DBEnvironment = ($_ | Select-Object -ExpandProperty TagList | Where-Object -Property Key -eq Environment).value
			DBEngine = $_.Engine
			DBStatus = $_.DBInstanceStatus
			DBAddress = $_.Endpoint.Address
			DBStorageSize = $_.AllocatedStorage
			DBCreateDate = $_.InstanceCreateTime
			DBBackupTime = $_.PreferredBackupWindow
			DBBacupRetention = $_.BackupRetentionPeriod
			DBSecurityGroups = $_.DBSecurityGroups.DBSecurityGroupName -join '+'
			VpcSecurityGroups = $_.VpcSecurityGroups.VpcSecurityGroupId -join '+'
			DBAvailabilityZone = $_.AvailabilityZone
			DBSubnets = $_.Subnets -join '+'
			DBLicenseModel = $_.LicenseModel
			DBiops = $_.iops
			DBPubliclyAccessible = $_.PubliclyAccessible
			DBClusterIdentifier = $_.DBClusterIdentifier
			DBCACertificateIdentifier = $_.CACertificateIdentifier
			# DBDomainMemberships  = $_.DomainMemberships.FQDN  -join '+'
			# DBProcessorCoreCount  = ($_.ProcessorFeatures | Where-Object -Property Name -eq coreCount).value
			DBTags = $_.TagList.key -join '+'
			DBCustomerOwnedIpEnabled = $_.CustomerOwnedIpEnabled
			
		}
		New-Object -TypeName PSObject -Property $properties
	}
	$RDSlistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-Dev-report.csv'
}

Get-RDSDev-info

function Get-RDSTest-info
{
	aws s3 ls --profile cts-test
	
	# Get all RDS Instances by profile and create list
	
	$RDSlist = (aws rds describe-db-instances --profile cts-test) | ConvertFrom-Json | Select-Object -ExpandProperty DBInstances
	
	
	# Parse Instance list to get data by instance
	$RDSlistdetails = $RDSlist | ForEach-Object {
		$properties = [ordered]@{
			DBName = $_.DBInstanceIdentifier
			DBEnvironment = ($_ | Select-Object -ExpandProperty TagList | Where-Object -Property Key -eq Environment).value
			DBEngine = $_.Engine
			DBStatus = $_.DBInstanceStatus
			DBAddress = $_.Endpoint.Address
			DBStorageSize = $_.AllocatedStorage
			DBCreateDate = $_.InstanceCreateTime
			DBBackupTime = $_.PreferredBackupWindow
			DBBacupRetention = $_.BackupRetentionPeriod
			DBSecurityGroups = $_.DBSecurityGroups.DBSecurityGroupName -join '+'
			VpcSecurityGroups = $_.VpcSecurityGroups.VpcSecurityGroupId -join '+'
			DBAvailabilityZone = $_.AvailabilityZone
			DBSubnets = $_.Subnets -join '+'
			DBLicenseModel = $_.LicenseModel
			DBiops = $_.iops
			DBPubliclyAccessible = $_.PubliclyAccessible
			DBClusterIdentifier = $_.DBClusterIdentifier
			DBCACertificateIdentifier = $_.CACertificateIdentifier
			# DBDomainMemberships  = $_.DomainMemberships.FQDN  -join '+'
			#DBProcessorCoreCount  = ($_.ProcessorFeatures | Where-Object -Property Name -eq coreCount).value
			DBTags = $_.TagList.key -join '+'
			DBCustomerOwnedIpEnabled = $_.CustomerOwnedIpEnabled
			
			
		}
		New-Object -TypeName PSObject -Property $properties
	}
	$RDSlistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-Test-report.csv'
}

Get-RDSTest-info

function Get-RDSUSProd-info
{
	aws s3 ls --profile cts-USProd
	
	# Get all RDS Instances by profile and create list
	
	$RDSlist = (aws rds describe-db-instances --profile cts-USProd) | ConvertFrom-Json | Select-Object -ExpandProperty DBInstances
	
	
	# Parse Instance list to get data by instance
	$RDSlistdetails = $RDSlist | ForEach-Object {
		$properties = [ordered]@{
			DBName = $_.DBInstanceIdentifier
			DBEnvironment = ($_ | Select-Object -ExpandProperty TagList | Where-Object -Property Key -eq Environment).value
			DBEngine = $_.Engine
			DBStatus = $_.DBInstanceStatus
			DBAddress = $_.Endpoint.Address
			DBStorageSize = $_.AllocatedStorage
			DBCreateDate = $_.InstanceCreateTime
			DBBackupTime = $_.PreferredBackupWindow
			DBBacupRetention = $_.BackupRetentionPeriod
			DBSecurityGroups = $_.DBSecurityGroups.DBSecurityGroupName -join '+'
			VpcSecurityGroups = $_.VpcSecurityGroups.VpcSecurityGroupId -join '+'
			DBAvailabilityZone = $_.AvailabilityZone
			DBSubnets = $_.Subnets -join '+'
			DBLicenseModel = $_.LicenseModel
			DBiops = $_.iops
			DBPubliclyAccessible = $_.PubliclyAccessible
			DBClusterIdentifier = $_.DBClusterIdentifier
			DBCACertificateIdentifier = $_.CACertificateIdentifier
			#DBDomainMemberships  = $_.DomainMemberships.FQDN  -join '+'
			#DBProcessorCoreCount  = ($_.ProcessorFeatures | Where-Object -Property Name -eq coreCount).value
			DBTags = $_.TagList.key -join '+'
			DBCustomerOwnedIpEnabled = $_.CustomerOwnedIpEnabled
			
			
		}
		New-Object -TypeName PSObject -Property $properties
	}
	$RDSlistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-USProd-report.csv'
}

Get-RDSUSProd-info

function Get-RDSCAProd-info
{
	aws s3 ls --profile cts-CAProd
	
	# Get all RDS Instances by profile and create list
	
	$RDSlist = (aws rds describe-db-instances --profile cts-CAProd) | ConvertFrom-Json | Select-Object -ExpandProperty DBInstances
	
	
	# Parse Instance list to get data by instance
	$RDSlistdetails = $RDSlist | ForEach-Object {
		$properties = [ordered]@{
			DBName = $_.DBInstanceIdentifier
			DBEnvironment = ($_ | Select-Object -ExpandProperty TagList | Where-Object -Property Key -eq Environment).value
			DBEngine = $_.Engine
			DBStatus = $_.DBInstanceStatus
			DBAddress = $_.Endpoint.Address
			DBStorageSize = $_.AllocatedStorage
			DBCreateDate = $_.InstanceCreateTime
			DBBackupTime = $_.PreferredBackupWindow
			DBBacupRetention = $_.BackupRetentionPeriod
			DBSecurityGroups = $_.DBSecurityGroups.DBSecurityGroupName -join '+'
			VpcSecurityGroups = $_.VpcSecurityGroups.VpcSecurityGroupId -join '+'
			DBAvailabilityZone = $_.AvailabilityZone
			DBSubnets = $_.Subnets -join '+'
			DBLicenseModel = $_.LicenseModel
			DBiops = $_.iops
			DBPubliclyAccessible = $_.PubliclyAccessible
			DBClusterIdentifier = $_.DBClusterIdentifier
			DBCACertificateIdentifier = $_.CACertificateIdentifier
			#DBDomainMemberships  = $_.DomainMemberships.FQDN  -join '+'
			#DBProcessorCoreCount  = ($_.ProcessorFeatures | Where-Object -Property Name -eq coreCount).value
			DBTags = $_.TagList.key -join '+'
			DBCustomerOwnedIpEnabled = $_.CustomerOwnedIpEnabled
			
			
		}
		New-Object -TypeName PSObject -Property $properties
	}
	$RDSlistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-CAProd-report.csv'
}

Get-RDSCAProd-info

$files = Get-ChildItem 'c:\temp\RDSReports\*.csv'
$files | ForEach-Object { Import-Csv $_ } | Export-Csv -NoTypeInformation 'c:\temp\RDSReports\merged\CompuCom-AWS-RDS-Report.csv'

Remove-Item -Path 'c:\temp\RDSReports\*.csv'


function Get-RDSSnaps-info
{
	aws s3 ls --profile cts-development
	
	# Get all RDS Snapshots by profile and create list
	
	$DBSnaps = (aws rds describe-db-snapshots --profile cts-development) | ConvertFrom-Json | Select-Object -ExpandProperty DBSnapshots
	
	$DBSnaplistdetails = $DBSnaps | ForEach-Object {
		$propertiesR = [ordered]@{
			
			DBInstanceName = $_.DBInstanceIdentifier
			DBSnapshotIdentifier = $_.DBSnapshotIdentifier
			DBSnapStorage  = $_.AllocatedStorage
			DbiResourceId  = $_.DbiResourceId
		}
		New-Object -TypeName PSObject -Property $propertiesR
	}
	$DBSnaplistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-DevSnap-report.csv'
}
Get-RDSSnaps-info

function Get-RDSSnapsTest-info
{
	aws s3 ls --profile cts-test
	
	# Get all RDS Snapshots by profile and create list
	
	$DBSnaps = (aws rds describe-db-snapshots --profile cts-test) | ConvertFrom-Json | Select-Object -ExpandProperty DBSnapshots
	
	$DBSnaplistdetails = $DBSnaps | ForEach-Object {
		$propertiesR = [ordered]@{
			
			DBInstanceName = $_.DBInstanceIdentifier
			DBSnapshotIdentifier = $_.DBSnapshotIdentifier
			DBSnapStorage  = $_.AllocatedStorage
			DbiResourceId  = $_.DbiResourceId
		}
		New-Object -TypeName PSObject -Property $propertiesR
	}
	$DBSnaplistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-TestSnap-report.csv'
}
Get-RDSSnapsTest-info

function Get-RDSSnapsUSProd-info
{
	aws s3 ls --profile cts-USProd
	
	# Get all RDS Snapshots by profile and create list
	
	$DBSnaps = (aws rds describe-db-snapshots --profile cts-USProd) | ConvertFrom-Json | Select-Object -ExpandProperty DBSnapshots
	
	$DBSnaplistdetails = $DBSnaps | ForEach-Object {
		$propertiesR = [ordered]@{
			
			DBInstanceName = $_.DBInstanceIdentifier
			DBSnapshotIdentifier = $_.DBSnapshotIdentifier
			DBSnapStorage  = $_.AllocatedStorage
			DbiResourceId  = $_.DbiResourceId
		}
		New-Object -TypeName PSObject -Property $propertiesR
	}
	$DBSnaplistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-USProdSnap-report.csv'
}
Get-RDSSnapsUSProd-info

function Get-RDSSnapsCAProd-info
{
	aws s3 ls --profile cts-CAProd
	
	# Get all RDS Snapshots by profile and create list
	
	$DBSnaps = (aws rds describe-db-snapshots --profile cts-CAProd) | ConvertFrom-Json | Select-Object -ExpandProperty DBSnapshots
	
	$DBSnaplistdetails = $DBSnaps | ForEach-Object {
		$propertiesR = [ordered]@{
			
			DBInstanceName = $_.DBInstanceIdentifier
			DBSnapshotIdentifier = $_.DBSnapshotIdentifier
			DBSnapStorage  = $_.AllocatedStorage
			DbiResourceId  = $_.DbiResourceId
		}
		New-Object -TypeName PSObject -Property $propertiesR
	}
	$DBSnaplistdetails | Sort-Object -Property DBName | Export-Csv -Path 'c:\temp\RDSReports\RDS-CAProdSnap-report.csv'
}
Get-RDSSnapsCAProd-info


$files = Get-ChildItem 'c:\temp\RDSReports\*.csv'
$files | ForEach-Object { Import-Csv $_ } | Export-Csv -NoTypeInformation 'c:\temp\RDSReports\merged\CompuCom-AWS-RDSSnap-Report.csv'

#

# SIG # Begin signature block
# MIIqTgYJKoZIhvcNAQcCoIIqPzCCKjsCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCABc7SFdeVxVeqf
# 6Sd5yyP8utUMxIRzZ7+3xeSDi5Swe6CCI+0wggNfMIICR6ADAgECAgsEAAAAAAEh
# WFMIojANBgkqhkiG9w0BAQsFADBMMSAwHgYDVQQLExdHbG9iYWxTaWduIFJvb3Qg
# Q0EgLSBSMzETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UEAxMKR2xvYmFsU2ln
# bjAeFw0wOTAzMTgxMDAwMDBaFw0yOTAzMTgxMDAwMDBaMEwxIDAeBgNVBAsTF0ds
# b2JhbFNpZ24gUm9vdCBDQSAtIFIzMRMwEQYDVQQKEwpHbG9iYWxTaWduMRMwEQYD
# VQQDEwpHbG9iYWxTaWduMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
# zCV2kHkGeCIW9cCDtoTKKJ79BXYRxa2IcvxGAkPHsoqdBF8kyy5L4WCCRuFSqwyB
# R3Bs3WTR6/Usow+CPQwrrpfXthSGEHm7OxOAd4wI4UnSamIvH176lmjfiSeVOJ8G
# 1z7JyyZZDXPesMjpJg6DFcbvW4vSBGDKSaYo9mk79svIKJHlnYphVzesdBTcdOA6
# 7nIvLpz70Lu/9T0A4QYz6IIrrlOmOhZzjN1BDiA6wLSnoemyT5AuMmDpV8u5BJJo
# aOU4JmB1sp93/5EU764gSfytQBVI0QIxYRleuJfvrXe3ZJp6v1/BE++bYvsNbOBU
# aRapA9pu6YOTcXbGaYWCFwIDAQABo0IwQDAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0T
# AQH/BAUwAwEB/zAdBgNVHQ4EFgQUj/BLf6guRSSuTVD6Y5qL3uLdG7wwDQYJKoZI
# hvcNAQELBQADggEBAEtA28BQqv7IDO/3llRFSbuWAAlBrLMThoYoBzPKa+Z0uboA
# La6kCtP18fEPir9zZ0qDx0R7eOCvbmxvAymOMzlFw47kuVdsqvwSluxTxi3kJGy5
# lGP73FNoZ1Y+g7jPNSHDyWj+ztrCU6rMkIrp8F1GjJXdelgoGi8d3s0AN0GP7URt
# 11Mol37zZwQeFdeKlrTT3kwnpEwbc3N29BeZwh96DuMtCK0KHCz/PKtVDg+Rfjbr
# w1dJvuEuLXxgi8NBURMjnc73MmuUAaiZ5ywzHzo7JdKGQM47LIZ4yWEvFLru21Vv
# 34TuBQlNvSjYcs7TYlBlHuuSl4Mx2bO1ykdYP18wggQ+MIIDJqADAgECAgRKU4wo
# MA0GCSqGSIb3DQEBCwUAMIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVz
# dCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJt
# czE5MDcGA1UECxMwKGMpIDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3Jp
# emVkIHVzZSBvbmx5MTIwMAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlv
# biBBdXRob3JpdHkgLSBHMjAeFw0wOTA3MDcxNzI1NTRaFw0zMDEyMDcxNzU1NTRa
# MIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVzdCwgSW5jLjEoMCYGA1UE
# CxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJtczE5MDcGA1UECxMwKGMp
# IDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3JpemVkIHVzZSBvbmx5MTIw
# MAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkgLSBH
# MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALqEtnLbngxr4pnpMAGn
# duoyuJVBGsnaYU5Ycs/+9oJ5v3NhBgqlJ9izX9NFThxy1k4y8nKKD/eDGdBqgIAA
# RR6wx+eavxJXJxyjaC8Kh71qaw5eZfMcd9XUhY1wIbSzMueLotWGOQKxuNJHzuTJ
# ScQ7p977VH1XvvDobsJ5sjoLVeJQmBYyE1wveFbBwpSz8lrkJ5qfJNfG7NCbJYLj
# zMLERcWMl3oGayoRn6kKbkg7b9vUERlC948Hv/VTX5w+9Bcs5mmsTjJMYnfqt+jl
# uzS8GYuunFHnt361U7EzIuVtz3A8Gvrim2e2g/SNpa9iTE3gWKxkNBID+LaNlGMk
# pHECAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
# VR0OBBYEFGpyJnrQHu995ztpUdRsjZ+QEmarMA0GCSqGSIb3DQEBCwUAA4IBAQB5
# nx2WxrZ5PyKNh9OHAwRgamuaLlmJcxGsQ9H1E/+NOSvA8r1PcIypL+oXxAtUntQb
# lpgzPKitYqIAdqtZaW4GHX7EuUSNmK8S1GHbChlGR/Pr92PBQAVApdK39LWaNr+p
# iHaIBFUEK5yHfxo3PH4tpRrY1Ileyr2sPWzYba/V83YPzTuIOCKdbJOaxD2/ghtl
# P6YPXar85bIVyrWtxrw90ITo6gZysE05Mni/PhGcC6SdmiHz8JsLMHjbwdyHQ/68
# Y5rKxcIcyceN/zsSWAjmtj3seixO+4OWzgw8aYdUc6RzwpP/URCsFVQB2PwFsYmh
# f3SDmknX3E57ikhvi0X2MIIFEjCCA/qgAwIBAgIQe8CWLo22wYoAAAAAVWaoxjAN
# BgkqhkiG9w0BAQsFADCBtDELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3Qs
# IEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMx
# OTA3BgNVBAsTMChjKSAyMDE1IEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXpl
# ZCB1c2Ugb25seTEoMCYGA1UEAxMfRW50cnVzdCBDb2RlIFNpZ25pbmcgQ0EgLSBP
# VkNTMTAeFw0xOTEwMjQxNjQ1MDRaFw0yMjEwMjQxNzE0NTRaMHAxCzAJBgNVBAYT
# AlVTMQ4wDAYDVQQIEwVUZXhhczEPMA0GA1UEBxMGRGFsbGFzMR8wHQYDVQQKExZD
# b21wdUNvbSBTeXN0ZW1zLCBJbmMuMR8wHQYDVQQDExZDb21wdUNvbSBTeXN0ZW1z
# LCBJbmMuMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA07NoB1Uer8oe
# WARuBnhwFjfAvvMVVFvehylhOiY7W/4wbT5zXjcIHBW2sjwsCuvzd3t2x1qNDSMU
# T+m+HcYuhJw7CihBfKExUXPn1DVzDiBSuV8ib1IkXA/8ydyahwY8neOqajziodH9
# l53/OlcCTlddDSeG8j01mSbPRbjkKgIoj4Q7wnyBhX8mi3GAhtXnEk+mQLhRvYNO
# eRnkyt5DOn1N0Z/DENMK6AEmTGaYRikdl2ecntNq7usDzfjhpRhCiA7X5Gn17xKm
# 76OO3SVsLG325BraWNkI/BEfi940ybdetmfbXSyVqIJ7w7hEG87gqi83TImB+6/4
# 2iLlKBp18QIDAQABo4IBYTCCAV0wDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoG
# CCsGAQUFBwMDMGoGCCsGAQUFBwEBBF4wXDAjBggrBgEFBQcwAYYXaHR0cDovL29j
# c3AuZW50cnVzdC5uZXQwNQYIKwYBBQUHMAKGKWh0dHA6Ly9haWEuZW50cnVzdC5u
# ZXQvb3ZjczEtY2hhaW4yNTYuY2VyMDEGA1UdHwQqMCgwJqAkoCKGIGh0dHA6Ly9j
# cmwuZW50cnVzdC5uZXQvb3ZjczEuY3JsMEwGA1UdIARFMEMwNwYKYIZIAYb6bAoB
# AzApMCcGCCsGAQUFBwIBFhtodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYSAwCAYG
# Z4EMAQQBMB8GA1UdIwQYMBaAFH4aHxoRdFxkyQwflAGr/YFkLqEsMB0GA1UdDgQW
# BBR4BhTXpSBb0vAxAGlmhCm2A6ALAzAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUA
# A4IBAQBlQj3DomT1S72glQqQ/2Dx1W/U4K0P3pSqKW+xepzV4gQ+Q+EkNDTPN57b
# i7WmjT7ORmeSRaddfb5m19X3LqsCA0tqpyEQKQSODD5J0lUL4/l2kUPxmY+fLva5
# ug2uCBk2jo3dfWnYU4VNSrZ13aCYa+T+eHHccPxGVD5tFkzXJbpOFpKbfBYpzbGj
# WyCBEKaTs/80mE2F9QG6Dp9DjSvOuIorWfSBkBLxy8xoEbIMfPtvDXRZpjuYlc/E
# /2gKF07JgC3bOsRt1dbgwrAy8yOw/O30xdDXklqflFeMk8aTq3X7CstAqcQSLaHn
# mwY5Ai1vKG88l2hJxuXtRgqN+0kXMIIFHTCCBAWgAwIBAgIMQ8ELHAAAAABR03Pa
# MA0GCSqGSIb3DQEBCwUAMIG+MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVz
# dCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJt
# czE5MDcGA1UECxMwKGMpIDIwMDkgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3Jp
# emVkIHVzZSBvbmx5MTIwMAYDVQQDEylFbnRydXN0IFJvb3QgQ2VydGlmaWNhdGlv
# biBBdXRob3JpdHkgLSBHMjAeFw0xNTA2MTAxMzQ2MDVaFw0zMDExMTAxNDE2MDVa
# MIG0MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNRW50cnVzdCwgSW5jLjEoMCYGA1UE
# CxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdhbC10ZXJtczE5MDcGA1UECxMwKGMp
# IDIwMTUgRW50cnVzdCwgSW5jLiAtIGZvciBhdXRob3JpemVkIHVzZSBvbmx5MSgw
# JgYDVQQDEx9FbnRydXN0IENvZGUgU2lnbmluZyBDQSAtIE9WQ1MxMIIBIjANBgkq
# hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2IYNoGQKavWOlro7mmhq20RA3vUt/Ott
# RFjPjnLDcC1eWtT5C9gRNZfAKfE77heh1kS5vx9KmOAZDrXjWQgdUoygsIooQaSh
# hkBQs03HiEHaNpOnTD1jLAPLr40AJRFpUCL2SwDOw7IugYXdI91OTbXwwKh0/Ots
# YCmYcY9o97cd8/hDi2KVMoZPyshMS1P+USAwHv8ARMdecZqlG4l0Sv5TPoTqsaOJ
# nfgtQljYCgZQI3AWHAJ3lxg82RB9DHrmBSwaCBZmEjhscR3WZNdWwI02w7i1ALjd
# L7V6v7Gkbnj6bRJ25Jl3EaYk5IK5RnmpsKNJW9NTdEOBdG+h+C4vJQIDAQABo4IB
# ITCCAR0wDgYDVR0PAQH/BAQDAgEGMBMGA1UdJQQMMAoGCCsGAQUFBwMDMBIGA1Ud
# EwEB/wQIMAYBAf8CAQAwMwYIKwYBBQUHAQEEJzAlMCMGCCsGAQUFBzABhhdodHRw
# Oi8vb2NzcC5lbnRydXN0Lm5ldDAwBgNVHR8EKTAnMCWgI6Ahhh9odHRwOi8vY3Js
# LmVudHJ1c3QubmV0L2cyY2EuY3JsMDsGA1UdIAQ0MDIwMAYEVR0gADAoMCYGCCsG
# AQUFBwIBFhpodHRwOi8vd3d3LmVudHJ1c3QubmV0L3JwYTAdBgNVHQ4EFgQUfhof
# GhF0XGTJDB+UAav9gWQuoSwwHwYDVR0jBBgwFoAUanImetAe733nO2lR1GyNn5AS
# ZqswDQYJKoZIhvcNAQELBQADggEBALd0Z7Q+TCUvYjWBnNJ25afiKHh0w+wZ5RP5
# tiOUT2KnTOZOcsIkkNB5uqjN10TfADkw8SjEcm94rJALLaPqha0UwgsUmT/7I3St
# qKrMImMyJQErytAWRkJap0RHWB0EQeLRMb6XYTuuBBhsenqBX6si/gjWNY6nDfXs
# INzcbceidtj7ZqhDAwEhqKD2TObUgs7XDQHWM6yQo+CSuxgCdUMCegS02BXtV5E8
# EueNZvi9FMxGgtb6A8AKDXyxgA/roRo2pXUfPNg66ueVgd6sWvudtcwRTip0EdZ/
# eUJhyiFBYB6k3fHZdLDVpegLTIGJ38yGFiUZpRRXlaKKInuxZY0wggVHMIIEL6AD
# AgECAg0B8kBCQM79ItvpbHH8MA0GCSqGSIb3DQEBDAUAMEwxIDAeBgNVBAsTF0ds
# b2JhbFNpZ24gUm9vdCBDQSAtIFIzMRMwEQYDVQQKEwpHbG9iYWxTaWduMRMwEQYD
# VQQDEwpHbG9iYWxTaWduMB4XDTE5MDIyMDAwMDAwMFoXDTI5MDMxODEwMDAwMFow
# TDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjYxEzARBgNVBAoTCkds
# b2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wggIiMA0GCSqGSIb3DQEBAQUA
# A4ICDwAwggIKAoICAQCVB+hzymb57BTKezz3DQjxtEULLIK0SMbrWzyug7hBkjMU
# pG9/6SrMxrCIa8W2idHGsv8UzlEUIexK3RtaxtaH7k06FQbtZGYLkoDKRN5zlE7z
# p4l/T3hjCMgSUG1CZi9NuXkoTVIaihqAtxmBDn7EirxkTCEcQ2jXPTyKxbJm1ZCa
# tzEGxb7ibTIGph75ueuqo7i/voJjUNDwGInf5A959eqiHyrScC5757yTu21T4kh8
# jBAHOP9msndhfuDqjDyqtKT285VKEgdt/Yyyic/QoGF3yFh0sNQjOvddOsqi250J
# 3l1ELZDxgc1Xkvp+vFAEYzTfa5MYvms2sjnkrCQ2t/DvthwTV5O23rL44oW3c6K4
# NapF8uCdNqFvVIrxclZuLojFUUJEFZTuo8U4lptOTloLR/MGNkl3MLxxN+Wm7CEI
# dfzmYRY/d9XZkZeECmzUAk10wBTt/Tn7g/JeFKEEsAvp/u6P4W4LsgizYWYJarEG
# OmWWWcDwNf3J2iiNGhGHcIEKqJp1HZ46hgUAntuA1iX53AWeJ1lMdjlb6vmlodiD
# D9H/3zAR+YXPM0j1ym1kFCx6WE/TSwhJxZVkGmMOeT31s4zKWK2cQkV5bg6HGVxU
# sWW2v4yb3BPpDW+4LtxnbsmLEbWEFIoAGXCDeZGXkdQaJ783HjIH2BRjPChMrwID
# AQABo4IBJjCCASIwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
# VR0OBBYEFK5sBaOTE+Ki5+LXHNbH8H/IZ1OgMB8GA1UdIwQYMBaAFI/wS3+oLkUk
# rk1Q+mOai97i3Ru8MD4GCCsGAQUFBwEBBDIwMDAuBggrBgEFBQcwAYYiaHR0cDov
# L29jc3AyLmdsb2JhbHNpZ24uY29tL3Jvb3RyMzA2BgNVHR8ELzAtMCugKaAnhiVo
# dHRwOi8vY3JsLmdsb2JhbHNpZ24uY29tL3Jvb3QtcjMuY3JsMEcGA1UdIARAMD4w
# PAYEVR0gADA0MDIGCCsGAQUFBwIBFiZodHRwczovL3d3dy5nbG9iYWxzaWduLmNv
# bS9yZXBvc2l0b3J5LzANBgkqhkiG9w0BAQwFAAOCAQEASaxexYPzWsthKk2XShUp
# n+QUkKoJ+cR6nzUYigozFW1yhyJOQT9tCp4YrtviX/yV0SyYFDuOwfA2WXnzjYHP
# dPYYpOThaM/vf2VZQunKVTm808Um7nE4+tchAw+3TtlbYGpDtH0J0GBh3artAF5O
# Mh7gsmyePLLCu5jTkHZqaa0a3KiJ2lhP0sKLMkrOVPs46TsHC3UKEdsLfCUn8awm
# zxFT5tzG4mE1MvTO3YPjGTrrwmijcgDIJDxOuFM8sRer5jUs+dNCKeZfYAOsQmGm
# sVdqM0LfNTGGyj43K9rE2iT1ThLytrm3R+q7IK1hFregM+Mtiae8szwBfyMagAk0
# 6TCCBlkwggRBoAMCAQICDQHsHJJA3v0uQF18R3QwDQYJKoZIhvcNAQEMBQAwTDEg
# MB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjYxEzARBgNVBAoTCkdsb2Jh
# bFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcNMTgwNjIwMDAwMDAwWhcNMzQx
# MjEwMDAwMDAwWjBbMQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBu
# di1zYTExMC8GA1UEAxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBTSEEz
# ODQgLSBHNDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAPAC4jAj+uAb
# 4Zp0s691g1+pR1LHYTpjfDkjeW10/DHkdBIZlvrOJ2JbrgeKJ+5Xo8Q17bM0x6zD
# DOuAZm3RKErBLLu5cPJyroz3mVpddq6/RKh8QSSOj7rFT/82QaunLf14TkOI/pMZ
# F9nuMc+8ijtuasSI8O6X9tzzGKBLmRwOh6cm4YjJoOWZ4p70nEw/XVvstu/SZc9F
# C1Q9sVRTB4uZbrhUmYqoMZI78np9/A5Y34Fq4bBsHmWCKtQhx5T+QpY78Quxf39G
# mA6HPXpl69FWqS69+1g9tYX6U5lNW3TtckuiDYI3GQzQq+pawe8P1Zm5P/RPNfGc
# D9M3E1LZJTTtlu/4Z+oIvo9Jev+QsdT3KRXX+Q1d1odDHnTEcCi0gHu9Kpu7hOEO
# rG8NubX2bVb+ih0JPiQOZybH/LINoJSwspTMe+Zn/qZYstTYQRLBVf1ukcW7sUwI
# S57UQgZvGxjVNupkrs799QXm4mbQDgUhrLERBiMZ5PsFNETqCK6dSWcRi4LlrVqG
# p2b9MwMB3pkl+XFu6ZxdAkxgPM8CjwH9cu6S8acS3kISTeypJuV3AqwOVwwJ0WGe
# Joj8yLJN22TwRZ+6wT9Uo9h2ApVsao3KIlz2DATjKfpLsBzTN3SE2R1mqzRzjx59
# fF6W1j0ZsJfqjFCRba9Xhn4QNx1rGhTfAgMBAAGjggEpMIIBJTAOBgNVHQ8BAf8E
# BAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQU6hbGaefjy1dFOTOk
# 8EC+0MO9ZZYwHwYDVR0jBBgwFoAUrmwFo5MT4qLn4tcc1sfwf8hnU6AwPgYIKwYB
# BQUHAQEEMjAwMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcDIuZ2xvYmFsc2lnbi5j
# b20vcm9vdHI2MDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly9jcmwuZ2xvYmFsc2ln
# bi5jb20vcm9vdC1yNi5jcmwwRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUH
# AgEWJmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMA0GCSqG
# SIb3DQEBDAUAA4ICAQB/4ojZV2crQl+BpwkLusS7KBhW1ky/2xsHcMb7CwmtADpg
# Mx85xhZrGUBJJQge5Jv31qQNjx6W8oaiF95Bv0/hvKvN7sAjjMaF/ksVJPkYROwf
# wqSs0LLP7MJWZR29f/begsi3n2HTtUZImJcCZ3oWlUrbYsbQswLMNEhFVd3s6Uqf
# XhTtchBxdnDSD5bz6jdXlJEYr9yNmTgZWMKpoX6ibhUm6rT5fyrn50hkaS/SmqFy
# 9vckS3RafXKGNbMCVx+LnPy7rEze+t5TTIP9ErG2SVVPdZ2sb0rILmq5yojDEjBO
# sghzn16h1pnO6X1LlizMFmsYzeRZN4YJLOJF1rLNboJ1pdqNHrdbL4guPX3x8pEw
# BZzOe3ygxayvUQbwEccdMMVRVmDofJU9IuPVCiRTJ5eA+kiJJyx54jzlmx7jqoSC
# iT7ASvUh/mIQ7R0w/PbM6kgnfIt1Qn9ry/Ola5UfBFg0ContglDk0Xuoyea+SKor
# VdmNtyUgDhtRoNRjqoPqbHJhSsn6Q8TGV8Wdtjywi7C5HDHvve8U2BRAbCAdwi3o
# C8aNbYy2ce1SIf4+9p+fORqurNIveiCx9KyqHeItFJ36lmodxjzK89kcv1NNpEdZ
# fJXEQ0H5JeIsEH6B+Q2Up33ytQn12GByQFCVINRDRL76oJXnIFm2eMakaqoimzCC
# BmUwggRNoAMCAQICEAGE06jON4HrV/T9h3uDrrIwDQYJKoZIhvcNAQELBQAwWzEL
# MAkGA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExMTAvBgNVBAMT
# KEdsb2JhbFNpZ24gVGltZXN0YW1waW5nIENBIC0gU0hBMzg0IC0gRzQwHhcNMjEw
# NTI3MTAwMDE2WhcNMzIwNjI4MTAwMDE1WjBjMQswCQYDVQQGEwJCRTEZMBcGA1UE
# CgwQR2xvYmFsU2lnbiBudi1zYTE5MDcGA1UEAwwwR2xvYmFsc2lnbiBUU0EgZm9y
# IE1TIEF1dGhlbnRpY29kZSBBZHZhbmNlZCAtIEc0MIIBojANBgkqhkiG9w0BAQEF
# AAOCAY8AMIIBigKCAYEA4qKbtkn7NEgoIweG/VITTbrLqjXi+UAcG3tpxZgqzMlc
# 7yfG7LglZcM/HUTn3uEI7wQXAJWXDP3bXYO0iOugjptPg7r5314YVGoCDg7mVPA3
# pB6cZzEFtTdxfHp+2IFlM+Sa6rAxN8dmtKO6BSfqblKCWNmCViSN+euzFPtkAKF7
# Kh96nqhjkebY0SLGP9KdOD+vlWlbJ7/oQCSZxx/Ze5IFAn0oxjw593rN92UmoDev
# MgV3l2rhEBuYhSyEreUlhnxzns2IjUsqep0PWXm98to2fmasLvi7iJfJbXnys9TV
# zBr0T4tFlXbyh5WAjih5uv3FcK+8GhQCkHwmWxGLXfW65BECxYEcZmhL3mjOnQyI
# Yaq2lCOd4KQ8Rl0EUds/WQvn7L7S2RMqR+9LEhJuyDXP7hYyttBfhbhP8nzYCSsS
# XsDC7xMxcDZz3NZXVNNXYJ4cEJXw5qovEIobfsnNj85QSxwlqMAXZFEVC4zP18ld
# SmxJdK5a8WoGdT6JNzuRAgMBAAGjggGbMIIBlzAOBgNVHQ8BAf8EBAMCB4AwFgYD
# VR0lAQH/BAwwCgYIKwYBBQUHAwgwHQYDVR0OBBYEFHWvJz+425rsbxzuw2pk9r+E
# NbDOMEwGA1UdIARFMEMwQQYJKwYBBAGgMgEeMDQwMgYIKwYBBQUHAgEWJmh0dHBz
# Oi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMAkGA1UdEwQCMAAwgZAG
# CCsGAQUFBwEBBIGDMIGAMDkGCCsGAQUFBzABhi1odHRwOi8vb2NzcC5nbG9iYWxz
# aWduLmNvbS9jYS9nc3RzYWNhc2hhMzg0ZzQwQwYIKwYBBQUHMAKGN2h0dHA6Ly9z
# ZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzdHNhY2FzaGEzODRnNC5jcnQw
# HwYDVR0jBBgwFoAU6hbGaefjy1dFOTOk8EC+0MO9ZZYwQQYDVR0fBDowODA2oDSg
# MoYwaHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9jYS9nc3RzYWNhc2hhMzg0ZzQu
# Y3JsMA0GCSqGSIb3DQEBCwUAA4ICAQA4k7d9NYk06hrWqnrOjYTb4TS3dKX/2F2U
# NiWP0oB+oWWxm881UV3boEtu9yFALuxPbxZAchx1GzAAFzmPyC+kcc4kMyaQ1eT/
# Oblh7wvKOfMtXo2g/07hVmQf1Jx2BKREXD1ihXAv+U6k94ZW23pJJkMtBZviw4m3
# OqEtJycYxkOL3EOhtnItP9nGNIoC02I5KfB9KNCpw2SKRm2zQxx0i925pgshexGn
# H12i24zgVdNp+nfRXRS5luyRRRsbbH5CQCSk7UDGZfpBj0gxnOO4sxdXgnnKzB7C
# 0E9PBQ4Neddp3ZWhcV2Knudawk+oBg8IiXvbWMglff9ovCiicJy7DLVT6I4GsbKC
# gYcSqV53TJOrGPhEV1gRy/aTFUkxt1NQIdrV/GB/3TDFrFFEC2el+tZ3NK0SHyj7
# iNpRm0Sa3ncO7jISYFYMkZ1Yj7yZV/K+zbnDcyQZ2nem5Ozx3m/gvIQdb8Px2+HV
# QlGGURJCJj7T6hP0KJytTXq3oroUaj9AVVhOjGUcs/Z11n+uD4SAL76IeFwnH0cX
# wj3kaZ7Lrp7vAmiINxDCbCaKyIxlkFNNrl3auEYQ1JAKiv/0KECB4FLI8gFkSBiE
# eGoi+sqcqg4dxY5ywzYiGev4lB+/wsUBVummgOoBHRM1B+K5dBT9mjieA9JJ9kzr
# ob0cFNLjmTGCBbcwggWzAgEBMIHJMIG0MQswCQYDVQQGEwJVUzEWMBQGA1UEChMN
# RW50cnVzdCwgSW5jLjEoMCYGA1UECxMfU2VlIHd3dy5lbnRydXN0Lm5ldC9sZWdh
# bC10ZXJtczE5MDcGA1UECxMwKGMpIDIwMTUgRW50cnVzdCwgSW5jLiAtIGZvciBh
# dXRob3JpemVkIHVzZSBvbmx5MSgwJgYDVQQDEx9FbnRydXN0IENvZGUgU2lnbmlu
# ZyBDQSAtIE9WQ1MxAhB7wJYujbbBigAAAABVZqjGMA0GCWCGSAFlAwQCAQUAoEww
# GQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwLwYJKoZIhvcNAQkEMSIEIJJqd4CC
# Xx9qKNpgaO90vO4vnX+7M/izeePDRqWX2w8AMA0GCSqGSIb3DQEBAQUABIIBAKIX
# ZIUdT9HZVbQ4ZBQjmqzypOH5bHJnzeBzJLyPR1Jros72QxRCsQ3AIo9eBOH30BvV
# byQdW6AM0MpZyIepitSMaxSUIy9AYmnILpI/7dxlVRHQdYgTcqOXZUilF/6oLYpz
# c3a1QniTuR3x5h5f+RxRiWwybdhsEuwgrTrFeH9+GYeneZOTMDzSCq3qTNimcf7w
# TXMj4QCKKPXKwE6gGLE4pBJPLaqkTjiKdOZpRByP23LcgVlDNUztZbBrtFqhHXHy
# twzvRc7VLVtzzKKb3QBGlKuU7ruWFLU1ArT+YvBX2LyxYrZgRHW5Nf1cE/o2hv9/
# 5kaYoezoFsmw8pCSw1GhggNwMIIDbAYJKoZIhvcNAQkGMYIDXTCCA1kCAQEwbzBb
# MQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBudi1zYTExMC8GA1UE
# AxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBTSEEzODQgLSBHNAIQAYTT
# qM43getX9P2He4OusjANBglghkgBZQMEAgEFAKCCAT8wGAYJKoZIhvcNAQkDMQsG
# CSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjExMDI3MjExMzEyWjAtBgkqhkiG
# 9w0BCTQxIDAeMA0GCWCGSAFlAwQCAQUAoQ0GCSqGSIb3DQEBCwUAMC8GCSqGSIb3
# DQEJBDEiBCB48QgymfRbWxNfl3RvS2UuE3+ZJEXC/AE+RtLAFsYHxzCBpAYLKoZI
# hvcNAQkQAgwxgZQwgZEwgY4wgYsEFN1XtbOHPIYbKcauxHMa++iNdcFJMHMwX6Rd
# MFsxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTEwLwYD
# VQQDEyhHbG9iYWxTaWduIFRpbWVzdGFtcGluZyBDQSAtIFNIQTM4NCAtIEc0AhAB
# hNOozjeB61f0/Yd7g66yMA0GCSqGSIb3DQEBCwUABIIBgJw+Beh4Tm6/Jblj4EAb
# afP/9gTjHMn1Joo4CYO1CW2pGIGlHxSoXgQk5M8s8YoThbCYG+Sv1ORBZfMkPsBh
# BMSASF9CUBxvFoGtP8CjU8VhE9oP0L7zmorLtTjCN+Kv+bI67aIpCWITpDyyVOQ6
# cvUoRMfGZJyW3rO3SLe1aPQfz0+nVNH+6eADYyd1VXdt/kwLKgyMMu5WOfOOMS1o
# 66Zm0KrEPZ3F/l5R1Xgnf3jiVoG3JhqIKEuunkqV4X3x7p3O7eMSkWv29ZLo7p15
# 2zF1nqmDlSVKDJKyiJt66C4YL4yeq+PzFVH/ikyIwCoy8cfqXX7saIjpabP3jHzt
# Bxas932TSz1joRZsSJXEDJ233VX8ewZ0X5tvVU4MT204ZiXlrQwoYpkuD1rzQ9Hh
# RWuOKxzN+mtBXjwBzkXjRpVmdbBLZ0wg+kJEFvOFt6Cg2v0E/wVdZ4KMwVvh0zVg
# 5CjxLS9elj8JUSmQ37fzU7zQvt/czfklGqGZX8JQj4z0tg==
# SIG # End signature block
